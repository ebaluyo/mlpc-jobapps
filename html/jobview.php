<?php
	session_start();
	require "lib/xmllib2.php";
	require "lib/xmlfuncs.php";
	require "lib/html_lib.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ML Consulting Pte Ltd, Singapore</title>
<link href="mlpc-css.css" rel="stylesheet" type="text/css" />
<link href="jobs.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="./flashJs/FLRelease1.js"> </script>
<script language="javascript" src="./flashJs/FLRelease2.js"> </script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>


<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginheight="0" marginwidth="0" style="background-image:url(images/bg-color.gif);">
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="main_bg">
  <!--DWLayoutTable-->
  <tr>
    <td height="151" colspan="5" align='center'><p><img src="images/name4.gif" height="105" width="585" /><span class="font4"><a href="http://www.mlpc.com.sg/index.html" class="nav3"><strong> <br />
        </strong></a></span><span class="font4">[ <a href="http://www.mlpc.com.sg/index.html" class="nav3" target="_top"><strong>Home</strong></a> ]</span></p>
    </td>
  </tr>
  <tr>
    <td width="5" height="9"></td>
    <td width="4"></td>
    <td width="249"></td>
    <td width="422"></td>
    <td width="24"></td>
  </tr>
  <tr class="font4">
    <td height="289"></td>
    <td colspan="4" rowspan="3" valign="top">
	<table width="95%"  align='center'><tr><td width="95%">
	<?php
	if (isset($_SESSION['auth'])) {
		echo "<hr>Welcome Back  ".$_SESSION['afname']."(".$_SESSION['auname'].") ";
		echo "[ <a href='joblogout.php'>Logout</a> ]"; //  [ <a href='appset.php'>Settings</a> ]";
		echo "<br/><hr>";
  	}
	?>
    Job Details
    <hr />
	<a href="#" onclick="window.history.go(-1)">&laquo; Back</a><br /><br />
	<?php
	//if ($_SESSION[jobadmin_auth]==1) {
		if (!empty($_GET["cat"]) && !empty($_GET["id"])) {
			if ($job = get_job($_GET["cat"], $_GET["id"])) {
	?>
		<table width="94%" cellspacing="2" cellpadding="2" align="left">
			<tr bgcolor="#e1e1e1">
				<th align="left" colspan="2"><?php echo stripslashes($job["jobtitle"]); ?></th>
			</tr>
			<tr valign="top">
				<td width="20%">Skills Required</td>
				<td width="80%"><?php echo newline_convert($job["jobskills"]); ?></td>
			</tr>
			<tr valign="top" bgcolor="#e1e1e1">
				<td>Job Description</td>
				<td><?php echo stripslashes(newline_convert($job["jobdesc"])); ?></td>
			</tr>
			<tr valign="top">
				<td>Requirements</td>
				<td><?php echo stripslashes(newline_convert($job["jobreq"])); ?></td>
			</tr>
			<tr valign="top" bgcolor="#e1e1e1">
				<td>Salary</td>
				<td><?php echo $job["jobsal"]; ?></td>
			</tr>
			<tr valign="top">
				<td>Location</td>
				<td><?php echo $job["jobloc"]; ?></td>
			</tr>
			<tr valign="top" bgcolor="#e1e1e1">
				<td>Job Type</td>
				<td><?php echo $job["jobtype"]; ?></td>
			</tr>
			<tr valign="top">
				<td>Job Category</td>
				<td><?php echo format_cat($job["cat"]); ?></td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<br />
					<form action="<?php echo isset($_SESSION['auth']) ? "apply.php" : "joblogin.php"; ?>" method="get">
						<input type="hidden" name="cat" value="<?php echo $job["cat"]; ?>" />
						<input type="hidden" name="id" value="<?php echo $job["id"]; ?>" />
						<input type="submit" value="A P P L Y" />
					</form>
				</td>
			</tr>
		</table>
	<?php
			} else echo "Cannot find job details.";
		} else echo "Invalid query!";
	//} else {
	//	include "jobadmin_login.php";
	//}
	?>
	</td></tr></table></td>
  </tr>
  <tr>
    <td height="58"></td>
  </tr>
  <tr>
    <td height="221"></td>
  </tr>
</table>
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="footer">
      <!--DWLayoutTable-->
      <tr>
        <td width="704" height="101" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
              <td width="704" height="101" align="center" valign="top" class="footer_font"><p>&copy; 2006 ML Consulting Pte Ltd, Singapore.</p>
              <p>This website is best viewed with Internet Explorer 6 and <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank" class="nav1">Flash Player 8</a> </p></td>
            </tr>
        </table></td>
  </tr>
</table>
</body>
</html>
