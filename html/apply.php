<?php
	session_start();
	require "lib/xmllib2.php";
	require "lib/html_lib.php";
	require "lib/xmlfuncs.php";

	if (!isset($_SESSION['auth'])) { // && $_SESSION['auth']!=1) {
		if (count($_POST) > 0) {
			if (
				empty($_POST["fullname"])
				|| empty($_FILES["resumefile"]["name"])
				|| empty($_POST["email"])
				|| empty($_POST["password"])
				|| $_POST["current_job"] < 0
				)
				$errormsg = "Please fill up all required fields.";
			else if ($_POST["password"] != $_POST["epassword"])
				$errormsg = "Passwords do not match.";
			else if (count($_POST["languages"]) > 5)
				$errormsg = "Maximum of 5 languages only.";
			else {
				$xmlfile = "xml/applicants.xml";
				$app = $_POST;
				$_languages = $app["languages"];
				$app["languages"] = array();
				$app["languages"]["language"] = $_languages;
				$email_exists = 0;
				
				//for file upload
				$userfile = $_FILES['resumefile'];
				$userfile_name = $_FILES['resumefile']['name'];
				$tmpname = $userfile['tmp_name']; 
					
					$time_stamp = "_".time();
					$the_file_name = str_replace(' ','_',$userfile_name);	
					$filename_arr = explode('.', $the_file_name);
					$last_index = (count($filename_arr)) - 1;
					$ext = '.'.$filename_arr[$last_index];
					$filename = '';
					for ($i = 0; $i < $last_index; $i++) {
						$filename .= $filename_arr[$i];
					}
					$destination_file = "resumes/".$filename.$time_stamp.$ext;
					$url_location = "resumes/".rawurlencode($filename).$time_stamp.$ext;
					$url_location_raw = "resumes/".rawurlencode($filename).$time_stamp.$ext;
					$name = safehtml(newline_convert($userfile_name['name']));
				
				if (copy($tmpname, $destination_file)) {
					$app["resumefile"] = $destination_file;
				}

				$applicants  = array();
				
				if (file_exists($xmlfile)) {
					$appsxml = XML_unserialize(file_get_contents($xmlfile));
					if ($appsxml) {						
						$lastID = $appsxml["application attr"]["last_id"];

						foreach ($appsxml["application"]["applicant"] as $_app) {
							if (isset($_app["email"]) && ($app["email"] == $_app["email"]))
								$email_exists = 1;
						}

						$applicants = $appsxml["application"]["applicant"];
					} else {
						//no contents; add dummy on top
						$lastID = 1; 					
						$applicants[] = array('id'=> '0', 'fullname' => "DO NOT REMOVE");
					}
					
				} else {
					$lastID = 1; 					
					$applicants[] = array('id'=> '0', 'fullname' => "DO NOT REMOVE");
				}
				
				if (!$email_exists) {
					$app['id'] = $lastID + 1;
					$applicants[] = $app;

					$appsxml["application attr"]["last_id"] = $lastID + 1;
					$appsxml["application"]["applicant"] = $applicants; 
					

					$fp = fopen($xmlfile, "w+");
					if (fwrite($fp, XML_serialize($appsxml)))
						//echo 'done saving';
						header("Location: joblogin.php?cat=".$_GET['cat']."&id=".$_GET['id']."&created=1");
					else
						die("Unexplainable error!");
				} else $errormsg = "The e-mail you supplied already exists in our database.";
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ML Consulting Pte Ltd, Singapore</title>
<link href="mlpc-css.css" rel="stylesheet" type="text/css" />
<link href="jobs.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="./flashJs/FLRelease1.js"> </script>
<script language="javascript" src="./flashJs/FLRelease2.js"> </script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>


<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginheight="0" marginwidth="0" style="background-image:url(images/bg-color.gif);">
<table width="704" border="0" align="center" cellpadding="0" cellspacing="0" class="main_bg">
  <!--DWLayoutTable-->
  <tr>
    <td height="151" colspan="5" align='center'><p><img src="images/name4.gif" height="105" width="585" /><span class="font4"><a href="http://www.mlpc.com.sg/index.html" class="nav3"><strong> <br />
    </strong></a></span><span class="font4">[<a href="http://www.mlpc.com.sg/index.html" class="nav3" target="_top"><strong> Home </strong>]</a></span></p></td>
  </tr>
  <tr>
    <td width="5" height="9"></td>
    <td width="4"></td>
    <td width="249"></td>
    <td width="422"></td>
    <td width="24"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="4" rowspan="3" valign="top">
	<table width="95%" align='center'><tr><td width="95%">
	<?php
	if (isset($_SESSION['auth']) && $_SESSION['auth']==1) {
		if (!empty($_GET["cat"]) && !empty($_GET["id"])) {
			if ($job = get_job($_GET["cat"], $_GET["id"])) {
				$app = get_app($_SESSION['auname']);
				// mail chuchu?>
			<hr />
			<span class="font4">Apply for <b><?php echo $job['jobtitle'].' (category: '.$_GET["cat"].' , ID:'.$_GET["id"].')'?></b>
            [ <a href="applyfor.php">Search Again</a> ]
			</span>
			<hr />
			<p class="font4">
			Dear <strong><u><?php echo $_SESSION['afname'] ?></u></strong>, <br/>
			<br/>

			Please review your job and job details below before submission. When you're done, you can click the Finish button below. </p>
			<table width="100%" border=0 cellspacing="2" cellpadding="2" align="center" style='font-family:Arial, Helvetica, sans-serif; font-size:11px'>
			<tr>
				<th height="18" colspan="2" align="left">&nbsp;</th>
			</tr>
			<tr valign="top">
				<td width="20%">Skills Required</td>
				<td width="80%"><?php echo newline_convert($job["jobskills"]); ?></td>
			</tr>
			<tr valign="top" bgcolor="#e1e1e1">
				<td>Job Description</td>
				<td><?php echo newline_convert($job["jobdesc"]); ?></td>
			</tr>
			<tr valign="top">
				<td>Requirements</td>
				<td><?php echo newline_convert($job["jobreq"]); ?></td>
			</tr>
			<tr valign="top" bgcolor="#e1e1e1">
				<td>Salary</td>
				<td><?php echo $job["jobsal"]; ?></td>
			</tr>
			<tr valign="top">
				<td>Location</td>
				<td><?php echo $job["jobloc"]; ?></td>
			</tr>
			<tr valign="top" bgcolor="#e1e1e1">
				<td>Job Type</td>
				<td><?php echo $job["jobtype"]; ?></td>
			</tr>
			<tr valign="top">
				<td>Job Category</td>
				<td><?php echo format_cat($job["cat"]); ?></td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<br />
				</td>
			</tr>
		</table>
		<br />
		<table width="100%" cellspacing="2" cellpadding="2" align="center"  style='font-family:Arial, Helvetica, sans-serif; font-size:11px'>
          <tr bgcolor="#e1e1e1">
            <th align="left" colspan="2"><?php echo (isset($app["jobtitle"]) ? $app["jobtitle"] : ''); #print_r($app);?></th>
          </tr>
          <tr valign="top">
            <td width="20%">FullName</td>
            <td width="80%"><?php echo newline_convert($app["fullname"]); ?></td>
          </tr>
          <tr valign="top" bgcolor="#e1e1e1">
            <td>City Name </td>
            <td><?php echo newline_convert($app["cityname"]); ?></td>
          </tr>
          <tr valign="top">
            <td>Resident</td>
            <td><?php echo newline_convert($app["resident"]); ?></td>
          </tr>
          <tr valign="top" bgcolor="#e1e1e1">
            <td>Phone</td>
            <td><?php echo newline_convert($app["phone"]); ?></td>
          </tr>
          <tr valign="top">
            <td>Mobile</td>
            <td><?php echo newline_convert($app["mobileno"]); ?></td>
          </tr>
          <tr valign="top" bgcolor="#e1e1e1">
            <td>Country</td>
            <td><?php echo newline_convert($app["country"]); ?></td>
          </tr>
          <tr valign="top">
            <td>Languages</td>
            <td><?php if (is_array($app["languages"]['language'])) echo implode(",",$app["languages"]['language']); ?></td>
          </tr>
          <tr>
            <td colspan="2" align="left">
            </td>
          </tr>
          <tr bgcolor="#e1e1e1">
            <td align="left">Expertise</td>
            <td align="left"><?php echo newline_convert($app["expertise"]); ?></td>
          </tr>
          <tr>
            <td align="left">Notice Period </td>
            <td align="left"><?php echo newline_convert($app["notice_period"]); ?></td>
          </tr>
          <tr bgcolor="#e1e1e1">
            <td align="left">Years Expirience</td>
            <td align="left"><?php echo newline_convert($app["years_expirience"]); ?></td>
          </tr>
          <tr>
            <td align="left">Current Job</td>
            <td align="left"><?php echo newline_convert($app["current_job"]); ?></td>
          </tr>
          <tr bgcolor="#e1e1e1">
            <td align="left">Ggender</td>
            <td align="left"><?php echo newline_convert($app["gender"]); ?></td>
          </tr>
          <tr>
            <td align="left">Disability</td>
            <td align="left"><?php echo newline_convert($app["disability"]); ?></td>
          </tr>
          <tr bgcolor="#e1e1e1">
            <td align="left">Race</td>
            <td align="left"><?php echo newline_convert($app["race"]); ?></td>
          </tr>
          <tr>
            <td align="left">Status</td>
            <td align="left"><?php echo newline_convert($app["status"]); ?></td>
          </tr>
          <tr>
            <td align="left">Resume File</td>
            <td align="left"><?php echo ($app["resumefile"] ? newline_convert($app["resumefile"]) : '[no resume file present]'); ?></td>
          </tr>
		  <tr><td height="30">&nbsp;</td></tr>
        </table>
		<form action="applyfinish.php" method="post">
			<input type="hidden" name="cat" value="<?php echo $_GET["cat"]; ?>" />
			<input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" />
			<input type="submit" value="Finish Application" />
		</form>
		<?php	} else die("Entry cannot be found.");
		} else die("Invalid query.");
	} else {
		include "lib/apply_form.php";
	} ?>
	    </td>
	</tr></table>
	</td>
  </tr>
  <tr>
    <td height="58"></td>
  </tr>
  <tr>
    <td height="221"></td>
  </tr>
</table>
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="footer">
  <!--DWLayoutTable-->
  <tr>
    <td width="704" height="101" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
          <td width="704" height="101" align="center" valign="top" class="footer_font"><p>&copy; 2006 ML Consulting Pte Ltd, Singapore.</p>
              <p>This website is best viewed with Internet Explorer 6 and <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank" class="nav1">Flash Player 8</a> </p></td>
        </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
