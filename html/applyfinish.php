<?php 
session_start() ;

require "lib/xmllib2.php";
require "lib/html_lib.php";
require "lib/xmlfuncs.php";
require 'lib/PHPMailerAutoload.php';		

function SendSMTP($from, $to, $subject, $msg, $filePath, $name) {
	//Create a new PHPMailer instance
	$mail = new PHPMailer();
	$mail->isSMTP();
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages			
	$mail->SMTPDebug = 0;
	
	$mail->Debugoutput = 'html';      //Ask for HTML-friendly debug output	
	#$mail->Host = "mail.mlpc.com.sg"; //Set the hostname of the mail server	
	#$mail->Port = 26;                 //Set the SMTP port number - likely to be 25, 465 or 587

	//SSL + SMTP
	$mail->Host = "icefish.vodien.com";	
	$mail->Port = 465;                 //Set the SMTP port number - likely to be 25, 465 or 587
	$mail->SMTPSecure = 'ssl';
	$mail->Username = "noreply@mlpc.com.sg"; //Username to use for SMTP authentication	
	$mail->Password = "NoReply!!"; //Password to use for SMTP authentication
	
	
	$mail->SMTPAuth = true;           //Whether to use SMTP authentication
	$mail->CharSet  = 'UTF-8';  // so it interprets foreign characters	
	
	$mail->setFrom('itapp@mlpc.com.sg', 'MLPC JobApp Admin'); //Set who the message is to be sent from	
	$mail->addReplyTo('itapp@mlpc.com.sg', 'MLPC JobApp Admin'); //Set an alternative reply-to address
	
	$mail->addAddress($to, $name); //Set who the message is to be sent to
	
	$mail->Subject = $subject; //Set the subject line

	$mail->msgHTML = nl2br($msg);
	$mail->Body = nl2br($msg);
	$mail->AltBody = $msg; //Replace the plain text body with one created manually

	#var_dump($mail); 
	$mail->addAttachment($filePath);

	if (!$mail->send()) {
    	//echo "Mailer Error: " . $mail->ErrorInfo;
    	return false;
	} else {
	    //echo "Message sent!";	    
	}

	return true;
}

	if (!isset($_SESSION['auth'])) { header("Location: applyfor.php"); }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ML Consulting Pte Ltd, Singapore</title>
<link href="mlpc-css.css" rel="stylesheet" type="text/css" />
<link href="jobs.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="./flashJs/FLRelease1.js"> </script>
<script language="javascript" src="./flashJs/FLRelease2.js"> </script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>


<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginheight="0" marginwidth="0" style="background-image:url(images/bg-color.gif);">
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="main_bg">
  <!--DWLayoutTable-->
  <tr>
    <td height="151" colspan="5" align='center'><p><img src="images/name4.gif" height="105" width="585" /><span class="font4"><a href="http://www.mlpc.com.sg/index.html" class="nav3"><strong> <br />
        </strong></a></span><span class="font4">[ <a href="http://www.mlpc.com.sg/index.html" class="nav3"><strong>Home</strong></a> ]</span></p>
    </td>
  </tr>
  <tr>
    <td width="5" height="9"></td>
    <td width="4"></td>
    <td width="249"></td>
    <td width="422"></td>
    <td width="24"></td>
  </tr>
  <tr>
    <td height="289"></td>
    <td colspan="4" rowspan="3" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <!--DWLayoutTable-->
      <tr>
        <td width="100%" align="left" valign="top" class="font4">
		<table width="95%" align='center'><tr><td width="95%">		
		<?php 
		  if (isset($_SESSION['auth'])) {
		  		echo "<hr>Welcome Back  ".$_SESSION['afname']."(".$_SESSION['auname'].") ";
				echo "[ <a href='joblogout.php'>Logout</a> ]"; //  [ <a href='appset.php'>Settings</a> ]";
				echo "<br/><hr>";
		  }
		  if ($job = get_job($_POST["cat"], $_POST["id"])) {
	            $app = get_app($_SESSION['auname']);

				$hrdept = " our HR Departpent for processing "; //$job["jobsendto"];
				$msg1 = "Dear Sir/Ma'm, \n\n Application for '".$job["jobtitle"]."' has been submitted by '".$app["fullname"]."'. \n\n Details below : \n\n\n"; 
				$msg2 = "Dear Applicant, \n\n Your Application for '".$job["jobtitle"]."' has been submitted to ".$hrdept.". \n\n Details below : \n\n\n"; 
	 
	            $msg = "Job Title : ".$job["jobtitle"]."\n".
	              "--------------------------------\n".
	              "Applicant : \n".
	              "--------------------------------\n".
	              "FullName  : ".newline_convert($app["fullname"])."\n".
				  "City Name : ".newline_convert($app["cityname"])."\n".
				  "Resident  : ".newline_convert($app["resident"])."\n".
	              "Phone     : ".newline_convert($app["phone"])."\n".
	              "Mobile    : ".newline_convert(isset($app["mobile"]) ? $app["mobile"] : '')."\n".
	              "Country   : ".newline_convert($app["country"])."\n".
	              "Languages : ".(is_array($app["languages"]["language"]) ? implode(',',$app["languages"]["language"]) : '')."\n".  
	              "Expertise : ".newline_convert($app["expertise"])."\n".
	              "Notice Period : ".newline_convert(isset($app["noticeperiod"]) ? $app["noticeperiod"] : '')."\n".
	              "Years Experience: ".newline_convert($app["years_expirience"])."\n".
	              "Current Job: ".newline_convert($app["current_job"])."\n".
	              "Gender    : ".newline_convert($app["gender"])."\n".
	              "Disability: ".newline_convert($app["disability"])."\n".
	              "Race      : ".newline_convert($app["race"])."\n".
	              "Status     : ".newline_convert($app["status"])."\n".
				  "Email     : ".newline_convert($app["email"])."\n\n\n";
	              	
			
				#$header1 = "From: itapp@mlpc.com.sg\r\nReply-To: ".$job["jobsendto"].$headers;            
				$subject1 = "Application Received: ".$job["jobtitle"].' - '.$app["fullname"];
				
				#$header2 = "From: itapp@mlpc.com.sg\r\nReply-To: ".$app["email"].$headers;
				$subject2 = "Application Sent: ".$job["jobtitle"].' - '.$app["fullname"];

				$fullPath = 'resumes/no-resume.txt';
				if (!empty($app['resumefile'])) {			  	
			  		if (file_exists($app['resumefile'])) {
			  			$fullPath = $app['resumefile'];
			  		} 
			  	}				

				$sent1 = SendSMTP($app["email"], 'itapp@mlpc.com.sg', $subject1, $msg1.$msg, $fullPath, 'Admin');
				$sent2 = SendSMTP($app["email"], $app["email"], $subject2, $msg2.$msg, $fullPath, $app["fullname"]);
				

				#if (@mail($job["jobsendto"], $subject1, $msg1.$msg, $header1)
				#	&& @mail($app["email"], $subject2, $msg2.$msg, $header2)) 
				echo "Application has been sent.". (!$sent1 ? '-' : '');
				echo 'Please wait for our HR Dept to contact you.'. (!$sent2 ? '-' : '') ;				
				
	          }
        ?>
		</td></tr></table>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="58"></td>
  </tr>
  <tr>
    <td height="221"></td>
  </tr>
</table>
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="footer">
      <!--DWLayoutTable-->
      <tr>
        <td width="704" height="101" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
              <td width="704" height="101" align="center" valign="top" class="footer_font"><p>&copy; 2006 ML Consulting Pte Ltd, Singapore.</p>
              <p>This website is best viewed with Internet Explorer 6 and <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank" class="nav1">Flash Player 8</a> </p></td>
            </tr>
        </table></td>
  </tr>
</table>
</body>
</html>