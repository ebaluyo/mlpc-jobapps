<?php session_start() ?>
<?php
	require "lib/xmllib2.php";
	require "lib/xmlfuncs.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ML Consulting Pte Ltd, Singapore</title>
<link href="mlpc-css.css" rel="stylesheet" type="text/css" />
<link href="jobs.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="./flashJs/FLRelease1.js"> </script>
<script language="javascript" src="./flashJs/FLRelease2.js"> </script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>


<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginheight="0" marginwidth="0" style="background-image:url(images/bg-color.gif);">
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="main_bg">
  <!--DWLayoutTable-->
  <tr>
    <td height="151" colspan="5" align='center'><p><img src="images/name4.gif" height="105" width="585" /><span class="font4"><a href="http://www.mlpc.com.sg/index.html" class="nav3"><strong> <br />
        </strong></a></span><span class="font4">[ <a href="http://www.mlpc.com.sg/index.html" class="nav3" target="_top"><strong>Home</strong></a> ]</span></p>
    </td>
  </tr>
  <tr>
    <td width="5" height="9"></td>
    <td width="4"></td>
    <td width="249"></td>
    <td width="422"></td>
    <td width="24"></td>
  </tr>
  <tr>
    <td height="289"></td>
    <td colspan="4" rowspan="3" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <!--DWLayoutTable-->
      <tr>
        <td width="100%" align="left" valign="top" class="font4">
		<table width="95%" align='center'><tr><td width="95%">
		<?php
		  if (isset($_SESSION['auth']) && $_SESSION['auth']) {
		  		echo "<hr>Welcome Back  ".$_SESSION['afname']."(".$_SESSION['auname'].") ";
				echo "[ <a href='joblogout.php'>Logout</a> ]"; //  [ <a href='appset.php'>Settings</a> ]";
				echo "<br/><hr>";
		  }
			if (!isset($_GET["jobkw"])) {
			$dir = "xml/";
			if ($dh = opendir($dir)) {
				$locations = array();
				$cats = array();
				while (($xmlfile = readdir($dh)) !== false) {
					if (is_cat_file($xmlfile)) {
						$jobsxml = XML_unserialize(file_get_contents($dir.$xmlfile));
						$jobs = $jobsxml["jobs"]["job"];
						if (is_array($jobs)) {  // cnt added new code
						$jobs = array_slice($jobs, 1);
						
						foreach ($jobs as $job) {
							if (!empty($job["jobloc"]))
								if (!in_array($job["jobloc"], $locations))
									$locations[] = $job["jobloc"];
						}
						
						}			// cnt added new code
						$cat = substr($xmlfile, 0, -4);
						$catf = str_replace(array("-", "_"), array("/", " "), $cat);
						$cats[$cat] = $catf;					}
				}
				closedir($dh);

			} else die("xml dir is missing!");
		  ?>
		  	<hr />
            <strong>Search Openings | ML Consulting Pte Ltd</strong>
			<hr />
            <p>Search our positions by selecting a location below. To see all openings sorted by location select "-Any-". Each job description includes a link for applying and submitting your resume to us online. This is the fastest and most reliable way to be considered for any of our positions. </p>
            <p> To fill out a general application form click <a href="apply.php">here</a>. </p>

            <form method="get">
            <table height="215" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="131" height="64" align="right" valign="top">Location:</td>
                  <td width="15" valign="top">&nbsp;</td>
                  <td width="531" valign="top"><select name="jobloc" size="3" id="jobloc" style="width:200px;">
                      <option value="any" selected="selected">--Any--</option>
                      <?php
	foreach ($locations as $location) {
		echo "<option value=\"$location\">$location</option>\n";
	}
?>
                  </select></td>
                </tr>
                <tr>
                  <td height="31" align="right" valign="top">Description Keywords:</td>
                  <td valign="top">&nbsp;</td>
                  <td valign="top"><input name="jobkw" type="text" id="jobkw" style="width:200px;"/></td>
                </tr>
                <tr>
                  <td height="88" align="right" valign="top">Job Function:</td>
                  <td valign="top">&nbsp;</td>
                  <td valign="top"><select name="jobcat" size="5" style="width:200px;">
                      <option value="all" selected="selected">All</option>
                      <?php
	foreach ($cats as $cat => $catf) {
		echo "\t<option value=\"$cat\">$catf</option>\n";
	}
?>
                  </select></td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                  <td valign="top">&nbsp;</td>
                  <td valign="top"><input type="submit" value="Search"/></td>
                </tr>
              </table>
            </form>
            <?php
	} else {
		echo "Job Search Results :: [ <a href='".$_SERVER['PHP_SELF']."'>&laquo; Search Again</a> ] <br><hr>";
		$jobloc = $_GET['jobloc'];
		$jobkw = $_GET['jobkw'];
		$jobcat = $_GET['jobcat'];

		include "lib/jobs_listxml.php";

	}
?>
            <hr />
            <?php if (!isset($_SESSION['auth'])) { ?>
            <p align='justify' style="background-color:lightblue;"> <font color="red"><strong>ADVANTAGES OF ONLINE RESUME SUBMISSION</strong></font>: Various Organizations have restructured their system of resume submission from the candidates to reduce the screening time. The candidate would need to input his/her contact details, select the appropriate pre-defined job function and submitted it along with the resume attached. Experts have found that this process takes just 30 seconds to complete the submission. In recent days, jobseekers welcome this as they were able to understand this system helps the employer/ recruiting firms to review one's profile to the core. </p>
            <p >You may alternatively forward your resumes to: </p>
            <ul>
              <li>IT Positions: <a href="mailto:itapp@mlpc.com.sg">itapp@mlpc.com.sg</a> </li>
              <li>Non IT Positions: <a href="mailto:adminapp@mlpc.com.sg">adminapp@mlpc.com.sg</a></li>
              <p></p>
            </ul>
            <hr />
            <a name="login"><p>Previous Applicants</p></a>
            <p>If you have previously applied to a position on our website, input your e-mail address and password below to login. </p>
            <form action='joblogin.php' method='post' name='login' id="login">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="145" height="19" valign="top" align="right">Email : </td>
                  <td width="259" valign="top"><input name="email" type="text" id="email" style="width:200px;"/></td>
                </tr>
                <tr>
                  <td height="31" valign="top" align="right">Password:</td>
                  <td valign="top"><input name="upass" type="password" id="upass" style="width:200px;"/></td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                  <td valign="top"><input type="submit" value="Login"/></td>
                </tr>
              </table>
            </form>
            <?php } ?>
		</td></tr></table>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="58"></td>
  </tr>
  <tr>
    <td height="221"></td>
  </tr>
</table>
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="footer">
      <!--DWLayoutTable-->
      <tr>
        <td width="704" height="101" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
              <td width="704" height="101" align="center" valign="top" class="footer_font"><p>&copy; 2006 ML Consulting Pte Ltd, Singapore.</p>
              <p>This website is best viewed with Internet Explorer 6 and <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank" class="nav1">Flash Player 8</a> </p></td>
            </tr>
        </table></td>
  </tr>
</table>
</body>
</html>
