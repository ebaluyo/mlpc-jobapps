<?php
//require "lib/class.xml.php";
require "lib/xmllib2.php";
session_start();

if (isset($_SESSION['auth']) && $_SESSION['auth']==1) {
	header("Location: applyfor.php");
}

if (!isset($_POST['email'])) {
//	$_SESSION['referrer'] = $_SERVER['HTTP_REFERER'];
//	echo $_SESSION['referrer'];
	$errormsg = "Please Login ";

} else {
	$auname = addslashes($_POST['email']);
	$aupass = addslashes($_POST['upass']);


	//print_r($_POST);

	$applicants = loadfile2('xml/applicants.xml'); //,"app");


	$apps = $applicants['application']['applicant'];

	$xSize = count($apps);



	for ($i=1;$i<$xSize;$i++) {
		//echo '<pre>';print_r($apps[$i]['email'].$apps[$i]['password']); echo '</pre>';//die;
		if ($apps[$i]['email']==$auname && $apps[$i]['password']== $aupass) {
			//echo $auname.'.'.$aupass;
			$row['auname'] = $apps[$i]['email'];
			$row['afname'] = $apps[$i]['fullname'];
			$xrow = $apps[$i];

			//echo '<pre>';print_r($xrow); echo '</pre>';die;
		}
	}

//	print_r($row['auname']);	die;
	$referrer = $_SERVER['HTTP_REFERER'];

	if ($row['auname']) {

		//print_r($row['auname']);	die;

		$_SESSION['auth'] = 1;
		$_SESSION['auname'] = $row['auname'];
		$_SESSION['afname'] = $row['afname'];

		if ($referrer != '') {
            if ($_GET[cat] && $_GET[id]) {
                header("Location: apply.php?cat=".$_GET[cat]."&id=".$_GET[id]);
            } else
			header("location: $referrer");
		} else {
            /*if ($_GET[cat] && $_GET[id]) {
                header("Location: apply.php?cat=".$_GET[cat]."&id=".$_GET[id]);
            } else*/
			header("Location: applyfor.php");
		}

	} else {
		$errormsg = " Invalid login, please try again";
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ML Consulting Pte Ltd, Singapore</title>
<link href="mlpc-css.css" rel="stylesheet" type="text/css" />
<link href="jobs.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="./flashJs/FLRelease1.js"> </script>
<script language="javascript" src="./flashJs/FLRelease2.js"> </script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>


<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginheight="0" marginwidth="0" style="background-image:url(images/bg-color.gif);">
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="main_bg">
  <!--DWLayoutTable-->
  <tr>
    <td height="151" colspan="5" align='center'><p><img src="images/name4.gif" height="105" width="585" /><span class="font4"><a href="http://www.mlpc.com.sg/index.html" class="nav3"><strong> <br />
        </strong></a></span><span class="font4">[ <a href="http://www.mlpc.com.sg/index.html" class="nav3" target="_top"><strong>Home</strong> ]</a></span></p>
    </td>
  </tr>
  <tr>
    <td width="5" height="9"></td>
    <td width="4"></td>
    <td width="249"></td>
    <td width="422"></td>
    <td width="24"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="4" rowspan="3" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
          <td width="100%" align="center" valign="top" class="font4">		  
		      <table width="95%" border="0" cellpadding="0" cellspacing="0">
            <tr><td>
              <?php if (isset($_GET['created'])) { echo "<p align='center' color='green'>Your account has been created.</p>"; } ?>
		          <?php if (isset($errormsg)) { echo "<p align='center' color='red'>$errormsg </p>"; } 
              $cat = (isset($_GET['cat']) ? $_GET['cat'] : '');
              $id  = (isset($_GET['id'])  ? $_GET['id'] : ''); 
              ?>
		          <p>Old users, if you have previously applied to a position on our website, input your e-mail address and password below to login.</p>
              <p>For new users 
              <a href="apply.php?cat=<?php echo $cat; ?>&amp;id=<?php echo $id; ?>">click here.</a></p>
              <form action='joblogin.php?cat=<?php echo $cat ?>&amp;id=<?php echo $id; ?>' method='post' name='login' id="login">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="145" height="19" valign="top" align="right">Email : </td>
                  <td width="259" valign="top"><input name="email" type="text" id="email" style="width:200px;"/></td>
                </tr>
                <tr>
                  <td height="31" valign="top" align="right">Password:</td>
                  <td valign="top"><input name="upass" type="password" id="upass" style="width:200px;"/></td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                  <td valign="top"><input name="submit" type="submit" value="Login"/></td>
                </tr>
              </table>
            </form>
            <hr />
			</td></tr></table>			
		  </td>
        </tr>
  </table></td>
  </tr>
  <tr>
    <td height="58"></td>
  </tr>
  <tr>
    <td height="221"></td>
  </tr>
</table>
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="footer">
      <!--DWLayoutTable-->
      <tr>
        <td width="704" height="101" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
              <td width="704" height="101" align="center" valign="top" class="footer_font"><p>&copy; 2006 ML Consulting Pte Ltd, Singapore.</p>
              <p>This website is best viewed with Internet Explorer 6 and <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank" class="nav1">Flash Player 8</a> </p></td>
            </tr>
        </table></td>
  </tr>
</table>
</body>
</html>
