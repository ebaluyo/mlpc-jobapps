<?php session_start(); 

	require "lib/asettings.php";
	
	if (isset($_SESSION['jobadmin_auth']) && $_SESSION['jobadmin_auth']!=1) { 		
		$errormsg = "Please Login";
		//include "lib/jaform.php";
	} 
	
	if (count($_POST) && isset($_POST['usern'])) {
		// check login if valid
		if ($_POST['usern']==$ADMIN['uname'] && $_POST['usern']==$ADMIN['passx']) {
			$_SESSION['jobadmin_auth']=1;
			$_SESSION['jobadmin_user']='Admin';			
		} 
		header("Location: jobadmin.php");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ML Consulting Pte Ltd, Singapore - Job Admin Pages</title>
<link href="mlpc-css.css" rel="stylesheet" type="text/css" />
<link href="jobs.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="./flashJs/FLRelease1.js"> </script>
<script language="javascript" src="./flashJs/FLRelease2.js"> </script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>


<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginheight="0" marginwidth="0" style="background-image:url(images/bg-color.gif);">
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="main_bg">
  <!--DWLayoutTable-->
  <tr>
    <td height="151" colspan="5" align="center"><p><img src="images/name4.gif" height="105" width="585" /><span class="font4"><a href="http://www.mlpc.com.sg/index.html" class="nav3"><strong> <br />
        </strong></a></span><span class="font4">[ <a href="http://www.mlpc.com.sg/index.html" class="nav3" target='_top'><strong>Home</strong></a>] </span></p>
    </td>
  </tr>
  <tr>
    <td width="5" height="9"></td>
    <td width="4"></td>
    <td width="249"></td>
    <td width="422"></td>
    <td width="24"></td>
  </tr>
  <tr>
    <td height="289"></td>
    <td colspan="4" rowspan="3" valign="top" class="font4">
	<table width="95%"  align='center'><tr><td width="95%">
		<hr />
		<?php if (isset($_SESSION['jobadmin_auth']) && $_SESSION['jobadmin_auth']==1) {  ?>
		[<a href="jobadmin.php">Job Admin</a>] [<a href="?act=add">Add New</a>] [<a href="?cat=all">Show All</a>] [<a href="joblogout.php">Logout</a>]
		<hr />
		<?php 
			include "lib/jobadmin_lib.php";
		} else {
			include "lib/jaform.php";
		}?>	
	</td></tr></table>	
	</td>
  </tr>
  <tr>
    <td height="58"></td>
  </tr>
  <tr>
    <td height="221"></td>
  </tr>
</table>
<table align="center" width="704" border="0" cellpadding="0" cellspacing="0" class="footer">
      <!--DWLayoutTable-->
      <tr>
        <td width="704" height="101" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
              <td width="704" height="101" align="center" valign="top" class="footer_font"><p>&copy; 2006 ML Consulting Pte Ltd, Singapore.</p>
              <p>This website is best viewed with Internet Explorer 6 and <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank" class="nav1">Flash Player 8</a> </p></td>
            </tr>
        </table></td>
  </tr>
</table>
</body>
</html>
