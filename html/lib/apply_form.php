<table width="100%" border="0" cellpadding="0" cellspacing="0">
      <!--DWLayoutTable-->
      <tr>
        <td width="100%" align="left" valign="top" class="font4"><table width="99%" border="0" cellspacing="0" cellpadding="0" align='center'>
            <tr>
              <td colspan="3">Application For Employment<br />
                  <hr />
                  [ <a href="javascript:history.go(-1);">&laquo; Back</a> ] [ <a href="applyfor.php">Jobs Home</a> ]</td>
            </tr>
            <tr>
              <td width="231">&nbsp;</td>
              <td width="192">&nbsp;</td>
              <td width="369">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" align="center" color="red"><?php echo (!empty($errormsg) ? $errormsg : ''); ?><br /><br /></td>
            </tr>
            <tr>
              <td colspan="3">To submit your application please complete the form below. Fields marked with a red asterisk <span style="color: red">*</span> are required. When you have finished click <strong>Submit</strong> at the bottom of this form </td>
            </tr>
          </table>
            <form action="apply.php?cat=<?php echo (isset($_GET['cat']) ? $_GET['cat'] : ''); ?>&id=<?php echo (isset($_GET['id']) ? $_GET['id'] : 0); ?>" method="post" enctype="multipart/form-data">
			  <input type="hidden" name="MAX_FILE_SIZE" value="1000000">		
              <table width="600" height="1187" border="0" align='center' cellpadding="0" cellspacing="0">
                <tr bgcolor="#e1e1e1">
                  <td height="18" colspan="5"><strong> Applicant Data</strong> </td>
                </tr>
                <tr>
                  <td width="106" align="right"><span style="color: red">*</span> Name: </td>
                  <td width="181"><input type="text" name="fullname" id="fullname" style="width:180px;"/></td>
                  <td width="6">&nbsp;</td>
                  <td width="121">NRIC / Passport / FIN No.: </td>
                  <td width="186"><input type="text" name="npf"  id="npf" style="width:180px;"/></td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td align="right"> City: </td>
                  <td><input type="text" name="cityname" id="cityname" style="width:180px;"/></td>
                  <td>&nbsp;</td>
                  <td>Phone #: </td>
                  <td><input type="text" name="phone" id="phone" style="width:180px;"/></td>
                </tr>
                <tr>
                  <td align="right"> Residency status: </td>
                  <td><select name="resident" id="resident" style="width:180px;">
                      <option value="">--Please select--</option>
                      <option value="Singaporean / PR">Singaporean / PR</option>
                      <option value="EP / S Pass / DP">EP / S Pass / DP</option>
                      <option value="Foreigner">Foreigner</option>
                      <option value="SVP">SVP</option>
                      <option value="Student Pass">Student Pass</option>
                  </select></td>
                  <td>&nbsp;</td>
                  <td>Mobile #: </td>
                  <td><input type="text" name="mobileno" id="mobileno" style="width:180px;"/></td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td align="right"> Country: </td>
                  <td><select name="country" id="country" style="width:180px;">
                      <option value="">--Please select--</option>
                      <option value="Afghanistan">Afghanistan</option>
                      <option value="Albania">Albania</option>
                      <option value="Algeria">Algeria</option>
                      <option value="American Samoa">American Samoa</option>
                      <option value="Andorra">Andorra</option>
                      <option value="Angola">Angola</option>
                      <option value="Anguilla">Anguilla</option>
                      <option value="Antarctica">Antarctica</option>
                      <option value="Antigua">Antigua</option>
                      <option value="Argentina">Argentina</option>
                      <option value="Armenia">Armenia</option>
                      <option value="Aruba">Aruba</option>
                      <option value="Australia">Australia</option>
                      <option value="Austria">Austria</option>
                      <option value="Azerbaijan">Azerbaijan</option>
                      <option value="Bahamas">Bahamas</option>
                      <option value="Bahrain">Bahrain</option>
                      <option value="Bangladesh">Bangladesh</option>
                      <option value="Barbados">Barbados</option>
                      <option value="Belarus">Belarus</option>
                      <option value="Belgium">Belgium</option>
                      <option value="Belize">Belize</option>
                      <option value="Benin">Benin</option>
                      <option value="Bermuda">Bermuda</option>
                      <option value="Bhutan">Bhutan</option>
                      <option value="Bolivia">Bolivia</option>
                      <option value="Bosnia">Bosnia</option>
                      <option value="Botswana">Botswana</option>
                      <option value="Brazil">Brazil</option>
                      <option value="Brunei Darussalam">Brunei Darussalam</option>
                      <option value="Bulgaria">Bulgaria</option>
                      <option value="Burkina Faso">Burkina Faso</option>
                      <option value="Burundi">Burundi</option>
                      <option value="Cambodia">Cambodia</option>
                      <option value="Cameroon">Cameroon</option>
                      <option value="Canada">Canada</option>
                      <option value="Cayman Islands">Cayman Islands</option>
                      <option value="Chad">Chad</option>
                      <option value="Chile">Chile</option>
                      <option value="China">China</option>
                      <option value="Colombia">Colombia</option>
                      <option value="Comoros">Comoros</option>
                      <option value="Congo">Congo</option>
                      <option value="Costa Rica">Costa Rica</option>
                      <option value="Croatia">Croatia</option>
                      <option value="Cuba">Cuba</option>
                      <option value="Cyprus">Cyprus</option>
                      <option value="Czech Republic">Czech Republic</option>
                      <option value="Denmark">Denmark</option>
                      <option value="Dominican Republic">Dominican Republic</option>
                      <option value="Ecuador">Ecuador</option>
                      <option value="Egypt">Egypt</option>
                      <option value="El Salvador">El Salvador</option>
                      <option value="Equatorial Guinea">Equatorial Guinea</option>
                      <option value="Eritrea">Eritrea</option>
                      <option value="Estonia">Estonia</option>
                      <option value="Ethiopia">Ethiopia</option>
                      <option value="Fiji">Fiji</option>
                      <option value="Finland">Finland</option>
                      <option value="France">France</option>
                      <option value="Gabon">Gabon</option>
                      <option value="Gambia">Gambia</option>
                      <option value="Georgia">Georgia</option>
                      <option value="Germany">Germany</option>
                      <option value="Ghana">Ghana</option>
                      <option value="Greece">Greece</option>
                      <option value="Greenland">Greenland</option>
                      <option value="Grenada">Grenada</option>
                      <option value="Guadeloupe">Guadeloupe</option>
                      <option value="Guam">Guam</option>
                      <option value="Guatemala">Guatemala</option>
                      <option value="Guinea">Guinea</option>
                      <option value="Guyana">Guyana</option>
                      <option value="Haiti">Haiti</option>
                      <option value="Honduras">Honduras</option>
                      <option value="Hong Kong">Hong Kong</option>
                      <option value="Hungary">Hungary</option>
                      <option value="Iceland">Iceland</option>
                      <option value="India">India</option>
                      <option value="Indonesia">Indonesia</option>
                      <option value="Iran">Iran</option>
                      <option value="Iraq">Iraq</option>
                      <option value="Ireland">Ireland</option>
                      <option value="Israel">Israel</option>
                      <option value="Italy">Italy</option>
                      <option value="Jamaica">Jamaica</option>
                      <option value="Japan">Japan</option>
                      <option value="Jordan">Jordan</option>
                      <option value="Kazakhstan">Kazakhstan</option>
                      <option value="Korea, North">Korea, North</option>
                      <option value="Korea, South">Korea, South</option>
                      <option value="Kuwait">Kuwait</option>
                      <option value="Kyrgyzstan">Kyrgyzstan</option>
                      <option value="Lao Democratic Republic">Lao Democratic Republic</option>
                      <option value="Latvia">Latvia</option>
                      <option value="Lebanon">Lebanon</option>
                      <option value="Liberia">Liberia</option>
                      <option value="Libya">Libya</option>
                      <option value="Liechtenstein">Liechtenstein</option>
                      <option value="Lithuania">Lithuania</option>
                      <option value="Luxembourg">Luxembourg</option>
                      <option value="Macao">Macao</option>
                      <option value="Macedonia">Macedonia</option>
                      <option value="Madagascar">Madagascar</option>
                      <option value="Malawi">Malawi</option>
                      <option value="Malaysia">Malaysia</option>
                      <option value="Maldives">Maldives</option>
                      <option value="Malta">Malta</option>
                      <option value="Mexico">Mexico</option>
                      <option value="Micronesia">Micronesia</option>
                      <option value="Moldova">Moldova</option>
                      <option value="Monaco">Monaco</option>
                      <option value="Mongolia">Mongolia</option>
                      <option value="Morocco">Morocco</option>
                      <option value="Mozambique">Mozambique</option>
                      <option value="Myanmar">Myanmar</option>
                      <option value="Namibia">Namibia</option>
                      <option value="Nepal">Nepal</option>
                      <option value="Netherlands">Netherlands</option>
                      <option value="New Zealand">New Zealand</option>
                      <option value="Nicaragua">Nicaragua</option>
                      <option value="Niger">Niger</option>
                      <option value="Nigeria">Nigeria</option>
                      <option value="Norway">Norway</option>
                      <option value="Oman">Oman</option>
                      <option value="Pakistan">Pakistan</option>
                      <option value="Palau">Palau</option>
                      <option value="Palestine">Palestine</option>
                      <option value="Panama">Panama</option>
                      <option value="Papua New Guinea">Papua New Guinea</option>
                      <option value="Paraguay">Paraguay</option>
                      <option value="Peru">Peru</option>
                      <option value="Philippines">Philippines</option>
                      <option value="Poland">Poland</option>
                      <option value="Portugal">Portugal</option>
                      <option value="Puerto Rico">Puerto Rico</option>
                      <option value="Qatar">Qatar</option>
                      <option value="Romania">Romania</option>
                      <option value="Russian Federation">Russian Federation</option>
                      <option value="Rwanda">Rwanda</option>
                      <option value="Saint Lucia">Saint Lucia</option>
                      <option value="Saint Vincent Grenadines">Saint Vincent Grenadines</option>
                      <option value="Samoa">Samoa</option>
                      <option value="San Marino">San Marino</option>
                      <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                      <option value="Saudi Arabia">Saudi Arabia</option>
                      <option value="Senegal">Senegal</option>
                      <option value="Serbia">Serbia</option>
                      <option value="Sierra Leone">Sierra Leone</option>
                      <option value="Singapore">Singapore</option>
                      <option value="Slovakia">Slovakia</option>
                      <option value="Slovenia">Slovenia</option>
                      <option value="Somalia">Somalia</option>
                      <option value="South Africa">South Africa</option>
                      <option value="Spain">Spain</option>
                      <option value="Sri Lanka">Sri Lanka</option>
                      <option value="Sudan">Sudan</option>
                      <option value="Suriname">Suriname</option>
                      <option value="Sweden">Sweden</option>
                      <option value="Switzerland">Switzerland</option>
                      <option value="Syria">Syria</option>
                      <option value="Taiwan">Taiwan</option>
                      <option value="Tajikistan">Tajikistan</option>
                      <option value="Tanzania">Tanzania</option>
                      <option value="Thailand">Thailand</option>
                      <option value="Togo">Togo</option>
                      <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                      <option value="Tunisia">Tunisia</option>
                      <option value="Turkey">Turkey</option>
                      <option value="Turkmenistan">Turkmenistan</option>
                      <option value="Tuvalu">Tuvalu</option>
                      <option value="Uganda">Uganda</option>
                      <option value="Ukraine">Ukraine</option>
                      <option value="United Arab Emirates">United Arab Emirates</option>
                      <option value="United Kingdom">United Kingdom</option>
                      <option value="United States">United States</option>
                      <option value="Uruguay">Uruguay</option>
                      <option value="Uzbekistan">Uzbekistan</option>
                      <option value="Vatican">Vatican</option>
                      <option value="Venezuela">Venezuela</option>
                      <option value="Vietnam">Vietnam</option>
                      <option value="Yemen">Yemen</option>
                      <option value="Zambia">Zambia</option>
                      <option value="Zimbabwe">Zimbabwe</option>
                  </select></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr align="left">
                  <td height="19" colspan="5">&nbsp;</td>
                </tr>
                <tr align="left">
                  <td height="64" colspan="5">Your email address will be used as your login name allowing you to return to our website to view your status and update your profile. If you do not have an email address, you can obtain a free account at Yahoo or Hotmail. Please make sure that the syntax of your email address is in the following form: username@ispname.com</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="19" align="right">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="23" align="right"><span style="color: red">*</span> Email: </td>
                  <td><input type="text" name="email" id="email" style="width:180px;"/></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td height="23" colspan="2" align="left"> Please create your password </td>
                  <td>&nbsp;</td>
                  <td colspan="2">Language (Maximum 5): </td>
                </tr>
                <tr>
                  <td height="23" align="left" bgcolor="#e1e1e1"><span style="color: red">*</span> Password: </td>
                  <td bgcolor="#e1e1e1"><input type="text" name="password" id="password" style="width:180px;"/></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td rowspan="4" valign="top"><select name="languages[]" id="language" size="5" multiple="multiple" style="width:180px;">
                      <option value="Arabic">Arabic</option>
                      <option value="Bengali">Bengali</option>
                      <option value="Cantonese">Cantonese</option>
                      <option value='English' selected="selected">English</option>
                      <option value="French">French</option>
                      <option value="German">German</option>
                      <option value="Hakka">Hakka</option>
                      <option value="Hindi">Hindi</option>
                      <option value="Indonesian">Indonesian</option>
                      <option value="Italian">Italian</option>
                      <option value="Japanese">Japanese</option>
                      <option value="Korean">Korean</option>
                      <option value="Malay">Malay</option>
                      <option value="Malayalam">Malayalam</option>
                      <option value="Mandarin">Mandarin</option>
                      <option value="Nepali">Nepali</option>
                      <option value="Portuguese">Portuguese</option>
                      <option value="Punjabi">Punjabi</option>
                      <option value="Spanish">Spanish</option>
                      <option value="Tagalog">Tagalog</option>
                      <option value="Tamil">Tamil</option>
                      <option value="Telugu">Telugu</option>
                      <option value="Teochew">Teochew</option>
                      <option value="Thai">Thai</option>
                      <option value="Vietnamese">Vietnamese</option>
                  </select></td>
                </tr>
                <tr>
                  <td height="38" align="left"><p><span style="color: red">*</span> Re-type <br>
                    New password:</p>
                  </td>
                  <td><input type="text" name="epassword" id="epassword" style="width:180px;"/></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td height="25" align="right">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td height="21" align="right">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="21" colspan="5" align="left"><strong>Pre-Requisites</strong> </td>
                </tr>
                <tr align="left">
                  <td height="34" colspan="5"> The following pre-requisites shall help us to match your profile to the core. Please enter your salary figures in Singapore Dollars. </td>
                </tr>
                <tr>
                  <td height="19" colspan="2" align="left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="27" colspan="2" align="left">Last Drawn Monthly Salary:</td>
                  <td>&nbsp;</td>
                  <td colspan="2">Expected Monthly Salary </td>
                </tr>
                <tr>
                  <td height="30" align="right"><table cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="22%" height="28" valign="top">&nbsp;</td>
                      </tr>
                  </table></td>
                  <td><input type="text" name="ldsalary" id="ldsalary" style="width:180px;"/></td>
                  <td>&nbsp;</td>
                  <td>&nbsp; </td>
                  <td><input type="text" name="esalary" id="esalary" style="width:180px;"/></td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="76" align="right" valign="top"> Expertise: </td>
                  <td><textarea name="expertise" id="expertise"></textarea></td>
                  <td>&nbsp;</td>
                  <td valign="top"> Notice Period: </td>
                  <td valign="top"><select name="notice_period" id="notice_period" style="width:180px;">
                      <option value="">--Please select--</option>
                      <option value="Immediate">Immediate</option>
                      <option value="1 week">1 week</option>
                      <option value="2 weeks">2 weeks</option>
                      <option value="3 weeks">3 weeks</option>
                      <option value="4 weeks">4 weeks</option>
                      <option value="1 - 2 months">1 - 2 months</option>
                      <option value="2 - 3 months">2 - 3 months</option>
                      <option value="Above 3 months">Above 3 months</option>

                  </select></td>
                </tr>
                <tr>
                  <td height="21" colspan="2" align="left"> Years of Experience: 
                  <input type="text" name="years_expirience" id="years_expirience" /></td>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="21" align="right">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr align="left">
                  <td height="21" colspan="5">&nbsp;</td>
                </tr>
                <tr align="left" bgcolor="#e1e1e1">
                  <td height="21" colspan="5"><strong> Resume &amp; Cover Letter</strong> </td>
                </tr>
                <tr align="left">
                  <td height="21" colspan="5"> You can paste a plain text version in the text area below. You can also use the text area for a cover letter and any supplementary information you would like to provide about your career goals, availability, best times to contact you, etc. </td>
                </tr>
                <tr>
                  <td height="21" colspan="5" align="left">&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="21" colspan="5" align="left"><span style="color: red">*</span> Current Job Function </td>
                </tr>
                <tr>
                  <td height="21" align="right" valign="top">&nbsp;</td>
                  <td colspan="4"><select name="current_job" id="current_job" style="width:250px;">
                    <option value="">--Please select--</option>
                    <option value="Accounting &gt; Accountant">Accounting &gt; Accountant</option>
                    <option value="Accounting &gt; Accounting Clerk/Supervisor">Accounting &gt; Accounting Clerk/Supervisor</option>
                    <option value="Accounting &gt; Chief Accountant">Accounting &gt; Chief Accountant</option>
                    <option value="Accounting &gt; Consulting">Accounting &gt; Consulting</option>
                    <option value="Accounting &gt; Credit Collection">Accounting &gt; Credit Collection</option>
                    <option value="Accounting &gt; External Audit">Accounting &gt; External Audit</option>
                    <option value="Accounting &gt; Finance/Accounting Manager">Accounting &gt; Finance/Accounting Manager</option>
                    <option value="Accounting &gt; Financial Controller">Accounting &gt; Financial Controller</option>
                    <option value="Accounting &gt; Internal Audit">Accounting &gt; Internal Audit</option>
                    <option value="Accounting &gt; Taxation">Accounting &gt; Taxation</option>
                    <option value="Accounting &gt; Others">Accounting &gt; Others</option>
                    <option value="Administration &gt; Clerical/Admin Assistant">Administration &gt; Clerical/Admin Assistant</option>
                    <option value="Administration &gt; Personal/Executive Assistant">Administration &gt; Personal/Executive Assistant</option>
                    <option value="Administration &gt; Receptionist">Administration &gt; Receptionist</option>
                    <option value="Administration &gt; Secretary">Administration &gt; Secretary</option>
                    <option value="Administration &gt; Others">Administration &gt; Others</option>
                    <option value="Engineering &gt; Aerospace &#38; Aviation">Engineering &gt; Aerospace &#38; Aviation</option>
                    <option value="Engineering &gt; Automotive">Engineering &gt; Automotive</option>
                    <option value="Engineering &gt; Chemical">Engineering &gt; Chemical</option>
                    <option value="Engineering &gt; Electrical &#38; Electronics">Engineering &gt; Electrical &#38; Electronics</option>
                    <option value="Engineering &gt; Energy / Oil &#38; Gas">Engineering &gt; Energy / Oil &#38; Gas</option>
                    <option value="Engineering &gt; Marine">Engineering &gt; Marine</option>
                    <option value="Engineering &gt; Mechanical">Engineering &gt; Mechanical</option>
                    <option value="Engineering &gt; Mechatronics">Engineering &gt; Mechatronics</option>
                    <option value="Engineering &gt; Process">Engineering &gt; Process</option>
                    <option value="Engineering &gt; Project Management">Engineering &gt; Project Management</option>
                    <option value="Engineering &gt; Technicians">Engineering &gt; Technicians</option>
                    <option value="Engineering &gt; Others">Engineering &gt; Others</option>
                    <option value="Education &#38; Training &gt; Early Childhood">Education &#38; Training &gt; Early Childhood</option>
                    <option value="Education &#38; Training &gt; Lecturer">Education &#38; Training &gt; Lecturer</option>
                    <option value="Education &#38; Training &gt; Librarian">Education &#38; Training &gt; Librarian</option>
                    <option value="Education &#38; Training &gt; Teacher">Education &#38; Training &gt; Teacher</option>
                    <option value="Education &#38; Training &gt; Tutor / Professor">Education &#38; Training &gt; Tutor / Professor</option>
                    <option value="Education &#38; Training &gt; Training &#38; Development">Education &#38; Training &gt; Training &#38; Development</option>
                    <option value="Education &#38; Training &gt; Others">Education &#38; Training &gt; Others</option>
                    <option value="Hospitality Services &gt; Hotel Svs - Concierge">Hospitality Services &gt; Hotel Svs - Concierge</option>
                    <option value="Hospitality Services &gt; Hotel Svs - Front Desk">Hospitality Services &gt; Hotel Svs - Front Desk</option>
                    <option value="Hospitality Services &gt; Hotel Svs - Researvation">Hospitality Services &gt; Hotel Svs - Researvation</option>
                    <option value="HR/Recruitment &gt; HR Director / Manager">HR/Recruitment &gt; HR Director / Manager</option>
                    <option value="HR/Recruitment &gt; HR Support Staff">HR/Recruitment &gt; HR Support Staff</option>
                    <option value="HR/Recruitment &gt; Recruitment / Executive Search">HR/Recruitment &gt; Recruitment / Executive Search</option>
                    <option value="HR/Recruitment &gt; Training &#38; Development">HR/Recruitment &gt; Training &#38; Development</option>
                    <option value="HR/Recruitment &gt; Others">HR/Recruitment &gt; Others</option>
                    <option value="IT &gt; Analysis &#38; Design">IT &gt; Analysis &#38; Design</option>
                    <option value="IT &gt; Architecture">IT &gt; Architecture</option>
                    <option value="IT &gt; Banking">IT &gt; Banking</option>
                    <option value="IT &gt; C, C++ / VB">IT &gt; C, C++ / VB</option>
                    <option value="IT &gt; Consulting">IT &gt; Consulting</option>
                    <option value="IT &gt; CRM Development">IT &gt; CRM Development</option>
                    <option value="IT &gt; Data Storage/Datawarehousing">IT &gt; Data Storage/Datawarehousing</option>
                    <option value="IT &gt; Database Administration">IT &gt; Database Administration</option>
                    <option value="IT &gt; DotNET/ C&#35; / ASP, E-commerce/Internet">IT &gt; DotNET/ C&#35; / ASP, E-commerce/Internet</option>
                    <option value="IT &gt; Embedded Systems/ Digital/VLSI/DSP">IT &gt; Embedded Systems/ Digital/VLSI/DSP</option>
                    <option value="IT &gt; ERP &#40;JD Edwards / PeopleSoft / Siebel&#41;">IT &gt; ERP &#40;JD Edwards / PeopleSoft / Siebel&#41;</option>
                    <option value="IT &gt; Groupware &#40;Lotus Notes/Exchange Server&#41;">IT &gt; Groupware &#40;Lotus Notes/Exchange Server&#41;</option>
                    <option value="IT &gt; Help Desk &#38; Support">IT &gt; Help Desk &#38; Support</option>
                    <option value="IT &gt; IT Management">IT &gt; IT Management</option>
                    <option value="IT &gt; IT Security / IT Audit">IT &gt; IT Security / IT Audit</option>
                    <option value="IT &gt; Java / J2EE">IT &gt; Java / J2EE</option>
                    <option value="IT &gt; Mainframe Developement">IT &gt; Mainframe Developement</option>
                    <option value="IT &gt; Marketing &#38; Business Development">IT &gt; Marketing &#38; Business Development</option>
                    <option value="IT &gt; Networking">IT &gt; Networking</option>
                    <option value="IT &gt; Product Management">IT &gt; Product Management</option>
                    <option value="IT &gt; Programming">IT &gt; Programming</option>
                    <option value="IT &gt; Project Management">IT &gt; Project Management</option>
                    <option value="IT &gt; QA &#38; Testing">IT &gt; QA &#38; Testing</option>
                    <option value="IT &gt; Sales/Account Management">IT &gt; Sales/Account Management</option>
                    <option value="IT &gt; SAP">IT &gt; SAP</option>
                    <option value="IT &gt; Oracle / PLSQL / SQL Development">IT &gt; Oracle / PLSQL / SQL Development</option>
                    <option value="IT &gt; Hardware/OS/Sys Admin">IT &gt; Hardware/OS/Sys Admin</option>
                    <option value="IT &gt; Technical Writing">IT &gt; Technical Writing</option>
                    <option value="IT &gt; Telecommunications">IT &gt; Telecommunications</option>
                    <option value="IT &gt; Temporary/Freelance/Part-Time">IT &gt; Temporary/Freelance/Part-Time</option>
                    <option value="IT &gt; Top/Senior Management">IT &gt; Top/Senior Management</option>
                    <option value="IT &gt; Training">IT &gt; Training</option>
                    <option value="IT &gt; Unix / Solaris / AIX">IT &gt; Unix / Solaris / AIX</option>
                    <option value="IT &gt; Web / Graphic / Video Design">IT &gt; Web / Graphic / Video Design</option>
                    <option value="Junior Executives &gt; Executive / Officer">Junior Executives &gt; Executive / Officer</option>
                    <option value="Junior Executives &gt; Management Trainee">Junior Executives &gt; Management Trainee</option>
                    <option value="Junior Executives &gt; Fresh Graduates / No Experienc">Junior Executives &gt; Fresh Graduates / No Experienc</option>
                    <option value="Management &gt; CFO">Management &gt; CFO</option>
                    <option value="Marketing/Public Relations &gt; Brand / Product Manag">Marketing/Public Relations &gt; Brand / Product Manag</option>
                    <option value="Marketing/Public Relations &gt; Business Development">Marketing/Public Relations &gt; Business Development</option>
                    <option value="Marketing/Public Relations &gt; Customer Service">Marketing/Public Relations &gt; Customer Service</option>
                    <option value="Marketing/Public Relations &gt; Direct Marketing">Marketing/Public Relations &gt; Direct Marketing</option>
                    <option value="Marketing/Public Relations &gt; General Marketing / S">Marketing/Public Relations &gt; General Marketing / S</option>
                    <option value="Marketing/Public Relations &gt; Market Research">Marketing/Public Relations &gt; Market Research</option>
                    <option value="Marketing/Public Relations &gt; Marketing Communicati">Marketing/Public Relations &gt; Marketing Communicati</option>
                    <option value="Marketing/Public Relations &gt; Copy-Writing">Marketing/Public Relations &gt; Copy-Writing</option>
                    <option value="Marketing/Public Relations &gt; Events Management">Marketing/Public Relations &gt; Events Management</option>
                    <option value="Marketing/Public Relations &gt; General PR / Support">Marketing/Public Relations &gt; General PR / Support</option>
                    <option value="Marketing/Public Relations &gt; Channel / Distributio">Marketing/Public Relations &gt; Channel / Distributio</option>
                    <option value="Marketing/Public Relations &gt; Sales Management">Marketing/Public Relations &gt; Sales Management</option>
                    <option value="Marketing/Public Relations &gt; Regional Sales">Marketing/Public Relations &gt; Regional Sales</option>
                    <option value="Marketing/Public Relations &gt; Technical Sales">Marketing/Public Relations &gt; Technical Sales</option>
                    <option value="Marketing/Public Relations &gt; Telesales">Marketing/Public Relations &gt; Telesales</option>
                    <option value="Marketing/Public Relations &gt; Wholesale">Marketing/Public Relations &gt; Wholesale</option>
                    <option value="Marketing/Public Relations &gt; Others">Marketing/Public Relations &gt; Others</option>
                    <option value="Research &#38; Survey &gt; Surveyors">Research &#38; Survey &gt; Surveyors</option>
                    <option value="Research &#38; Survey &gt; Project Co-ordinator">Research &#38; Survey &gt; Project Co-ordinator</option>
                    <option value="Research &#38; Survey &gt; Others">Research &#38; Survey &gt; Others</option>
                    <option value="Supply Chain &gt; Fulfillment">Supply Chain &gt; Fulfillment</option>
                    <option value="Supply Chain &gt; Inventory / Warehousing">Supply Chain &gt; Inventory / Warehousing</option>
                    <option value="Supply Chain &gt; Supply Chain">Supply Chain &gt; Supply Chain</option>
                    <option value="Supply Chain &gt; Freight Forwarding">Supply Chain &gt; Freight Forwarding</option>
                    <option value="Supply Chain &gt; Procurement / Purchasing / Sourcing">Supply Chain &gt; Procurement / Purchasing / Sourcing</option>
                    <option value="Supply Chain &gt; Transportation">Supply Chain &gt; Transportation</option>
                    <option value="Supply Chain &gt; Shipping">Supply Chain &gt; Shipping</option>
                    <option value="Supply Chain &gt; Others">Supply Chain &gt; Others</option>
                    <option value="Others &gt; Civil Services">Others &gt; Civil Services</option>
                    <option value="Others &gt; Security / Safety Control">Others &gt; Security / Safety Control</option>
                    <option value="Others &gt; Skilled Workers">Others &gt; Skilled Workers</option>
                    <option value="Others &gt; Translation &#38; Interpretation">Others &gt; Translation &#38; Interpretation</option>
                    <option value="Others &gt; Unskilled Workers">Others &gt; Unskilled Workers</option>
                    <option value="Others &gt; Others">Others &gt; Others</option>
                  </select></td>
                </tr>
                <tr align="left" bgcolor="#e1e1e1">
                  <td height="21" colspan="5" valign="top">Cover Letter / Text Resume:<strong>:</strong></td>
                </tr>
                <tr>
                  <td height="21" align="right" valign="top"><strong> <br>
                  <br />
              </strong></td>
                  <td colspan="4"><textarea name="text_resume" id="text_resume" rows="5" cols="50"></textarea></td>
                </tr>
                <tr>
                  <td height="21" align="left" valign="top" bgcolor="#e1e1e1">&nbsp;</td>
                  <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                  <td height="21" align="left" valign="top" bgcolor="#e1e1e1"><span style="color: red">*</span>  Attach resume: </td>
                  <td colspan="4" bgcolor="#e1e1e1"><input type="file" name="resumefile"> </td>
                </tr>
                <tr>
                  <td height="21" colspan="5" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="21" colspan="5" align="left" valign="top"><strong> Voluntary Equal Opportunity Questionnaire</strong> </td>
                </tr>
                <tr>
                  <td height="47" colspan="5" align="left" valign="top"> As most of our clients are equal opportunity employers, we hire without consideration to race, religion, creed, color, national origin, age, gender, sexual orientation, marital status, veteran status or disability. We invite you to complete the optional self-identification fields below used for compliance with government regulations and record-keeping guidelines. </td>
                </tr>
                <tr>
                  <td height="21" align="right" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="21" align="right" valign="top"> Gender: </td>
                  <td height="21" align="left" valign="top"><select name="gender" id="gender" style="width:180px;">
                    <option value="">--Please select--</option>
                    <option value="Female">Female</option>
                    <option value="Male">Male</option>
                  </select></td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top"> Veteran/Disability: </td>
                  <td height="21" align="left" valign="top"><select name="disability" id="disability" style="width:180px;">
                      <option value="">--Please select--</option>
                      <option value="None">None</option>
                      <option value="Vietnam War Veteran">Vietnam War Veteran</option>
                      <option value="Special Disabled Veteran">Special Disabled Veteran</option>
                      <option value="Other War Veteran">Other War Veteran</option>
                      <option value="Disabled">Disabled</option>
                  </select></td>
                </tr>
                <tr>
                  <td height="15" align="left" valign="top">&nbsp;</td>
                  <td height="15" align="left" valign="top">&nbsp;</td>
                  <td height="15" align="left" valign="top">&nbsp;</td>
                  <td height="15" align="left" valign="top">&nbsp;</td>
                  <td height="15" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="21" align="right" valign="top"> Race: </td>
                  <td height="21" align="left" valign="top"><select name="race" id="race" style="width:180px;">
                      <option value="">--Please select--</option>
                      <option value="Chinese">Chinese</option>
                      <option value="Eurasian">Eurasian</option>
                      <option value="Indian">Indian</option>
                      <option value="Malay">Malay</option>
                      <option value="Others">Others</option>
                  </select></td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top"> Marital Status: </td>
                  <td height="21" align="left" valign="top"><select name="status" id="status" style="width:180px;">
                      <option value="">--Please select--</option>
                      <option value="Divorced">Divorced</option>
                      <option value="Married">Married</option>
                      <option value="Single">Single</option>
                      <option value="Widowed">Widowed</option>
                  </select></td>
                </tr>
                <tr>
                  <td height="21" align="right" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr bgcolor="#e1e1e1">
                  <td height="21" align="right" valign="top"><input type="submit" value="S a v e" id="save" /></td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                  <td height="21" align="left" valign="top">&nbsp;</td>
                </tr>
              </table>
          </form></td>
      </tr>
    </table>
