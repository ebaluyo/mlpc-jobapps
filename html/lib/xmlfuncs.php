<?php
function d($arr, $die = 1, $vdump = 0) {
	echo "<pre style=\"font-size:12px; border:1px solid green; width:50%;\">";
	if (is_array($arr)) {
		#if ($vdump) 
			var_dump($arr);
		#else
		#	print_r($arr);			
	} else {
		echo $arr;
	}
	echo "</pre>";
	if ($die) die;
}

function is_cat_file($fn) {
	$noncat = array(
		".",
		"..",
		"applicants.xml"
		);
	if (in_array($fn, $noncat))
		return 0;
	return 1;
}

function get_app($email, $dir = "xml") {
	$xmlfile = "$dir/applicants.xml";
	if (file_exists($xmlfile)) {
		$appsxml = XML_unserialize(file_get_contents($xmlfile));
		$apps = $appsxml["application"]["applicant"];
		$apps = array_slice($apps, 1);
	} else return false;
	foreach ($apps as $app) {
		if ($app["email"] == $email)
			break;
	}
	if ($app["email"] == $email)
		return $app;
	return false;
}

function get_jobs($cat, $dir = "xml") {
	if ($cat == "all") {
		if ($dh = opendir($dir)) {
			$jobs = array();
			while (($xmlfile = readdir($dh)) !== false) {
				if (is_cat_file($xmlfile)) {
					$jobsxml = XML_unserialize(file_get_contents("$dir/$xmlfile")); 
					$_jobs = null;
					
					if (isset($jobsxml["jobs"]["job"])) $_jobs = $jobsxml["jobs"]["job"];  

					if ($_jobs != NULL) {
						#echo ' JOBS:'.$xmlfile.' jobCount: '.count($_jobs).'<br/>'; 						
						#d($_jobs, 0); 
						if (count($_jobs) > 1) {
							$_jobs = array_slice($_jobs, 1); // to remove dummy						
						}

						
                        $jobs = doMerge($jobs, $_jobs);
                        
                        //before merge
                        //echo 'before:'. print_r($jobs, true); //die;
                        //echo 'before:'. count($jobs) . '----------' . "\n"; //die;
                        // $jobs = doMerge($jobs, $_jobs);
                        //echo 'after:'. count($jobs) . '----------' . "\n"; //die;
                        //echo 'after:'. print_r($jobs, true) . '----------' . "\n"; //die;
					}
				}
			}

			#echo 'JOBS:'.$xmlfile; d($jobs, 1); echo '<br/>';
			closedir($dh);
		} else return false;
	} else {
		$xmlfile = "$dir/$cat.xml";
		if (file_exists($xmlfile)) {
			$jobsxml = XML_unserialize(file_get_contents($xmlfile)); 
			//d($jobsxml, 1);
			if (count($jobsxml) > 0) {
				$jobs = $jobsxml["jobs"]["job"];
				$jobs = array_slice($jobs, 1); 
			} else
				return false;
		} else return false;
	}
	
    //echo 'final:'. count($jobs) . '----------' . "\n"; //die;
	return $jobs;
}

function doMerge($arr1, $arr2) {
	if (is_array($arr1)) {
		$arrFinal = $arr1;
		if (is_array($arr2)) {
			foreach ($arr2 as $item) {
				$arrFinal[] = $item;
			}
		}
	} else { 
		$arrFinal = array(); 
	}

	return $arrFinal;
}
function get_job($cat, $id, $dir = "xml") {
	if ($jobs = get_jobs($cat, $dir)) {
		foreach ($jobs as $job) {
			if ($job["id"] == $id)
				break;
		}
		if ($job["id"] == $id)
			return $job;
	}
	return false;
}

function clean_cat($str) {
	return str_replace(array("/", " "), array("-", "_"), $str);
}

function format_cat($str) {
	return str_replace(array("-", "_"), array("/", " "), $str);
}

function backup_xmlfile($xmlfile) {
	$target = str_replace(array('xml/', '.xml'), array ('xmlbak/', '_'.time().'.xml'), $xmlfile); 
	copy($xmlfile, $target); 
	return true;
}