<?php
	if ($_GET["act"] == "edit" && (empty($cat) || empty($id))) die("Incomplete query.");
?>
<script language="javascript" type="text/javascript">
function formValidate() {
	var jobform = document.getElementById('jobform');
	if (
		jobform.jobtitle.value.length < 1
		|| jobform.jobdesc.value.length < 1
		|| jobform.jobreq.value.length < 1
		)
	{
		alert("Please fill up all required fields.");
    if (jobform.jobtitle.value.length < 1) jobform.jobtitle.focus();    
    else if (jobform.jobdesc.value.length < 1) jobform.jobdesc.focus();
    else if (jobform.jobreq.value.length < 1) jobform.jobreq.focus();
	}
	else {
		jobform.submit();
	}
}
window.onload = function() {
	var cats = document.getElementById('jobform').cat;
	cats[0].checked = true; // set first category as default
<?php if ($_GET["act"] == "edit"): ?>
	for (var i = 0; i < cats.length; i++) {
		if (cats[i].value == '<?php echo $cat; ?>')
			cats[i].checked = true;
	}
<?php endif; ?>
}
</script>
<form method="post" action="jobadmin.php?act=<?php echo $_GET["act"]; ?>" id="jobform">
<?php
	$job = array(
		"jobtitle" => "",
		"jobskills" => "",
		"jobdesc" => "",
		"jobreq" => "",
		"jobsal" => "",
		"jobloc" => "",
		"jobtype" => "",
		"jobsendto" => ""
		);
	if ($_GET["act"] == "edit") {
		echo "<input type=\"hidden\" name=\"id\" value=\"$id\" />\n";
		$xmlfile = "xml/".$cat.".xml";
		if (file_exists($xmlfile)) {
			$jobsxml = XML_unserialize(file_get_contents($xmlfile));
			$jobs = $jobsxml["jobs"]["job"];
		}
		foreach ($jobs as $_job) {
			if ($_job["id"] == $id) $job = $_job;
		}
	}
?>
<table width="627" cellpadding="0" cellspacing="0" align='center' border='0'>
  <tr bgcolor="#e1e1e1">
    <td width="140" height="30" align="right" valign="middle" bgcolor="#e1e1e1"><strong> Job Title <font color=red>*</font> </strong></td>
    <td valign="top">&nbsp;</td>
    <td valign="middle" bgcolor="#e1e1e1"><input type="text" name="jobtitle" size="80" value="<?php echo $job["jobtitle"]; ?>" /></td>
  </tr>
  <tr>
    <td height="19" align="right" valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td width="140" height="32" align="right" valign="middle" bgcolor="#e1e1e1"><strong> Skills Required </strong></td>
    <td valign="top">&nbsp;</td>
    <td valign="middle" bgcolor="#e1e1e1">
      <input type="text" name="jobskills" size="80" value="<?php echo $job["jobskills"]; ?>" /></td>
  </tr>
  <tr>
    <td height="19" align="right" valign="top" >&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td align="right" valign="top" ><strong> Job Description </strong><font color=red>*</font>
      </td>
    <td valign="top">&nbsp;</td>
    <td valign="top"><strong>
      <textarea name="jobdesc" cols="55" rows="15"><?php echo html_entity_decode(stripslashes($job["jobdesc"])); ?></textarea>
    </strong></td>
  </tr>
  <tr>
    <td height="19" align="right" valign="top" >&nbsp;</td>
    <td >&nbsp;</td>
    <td >&nbsp;</td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td height="197" align="right" valign="top" ><strong> Requirements </strong><font color=red>*</font>
        <strong>&nbsp;
        </strong>        
    </td>
    <td >&nbsp;</td>
    <td ><strong>
      <textarea name="jobreq" cols="55" rows="10"><?php echo html_entity_decode(stripslashes($job["jobreq"])); ?></textarea>
    </strong></td>
  </tr>
  <tr>
    <td align="right" valign="top" >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td height="32" align="right" valign="middle" bgcolor="#e1e1e1" ><strong>Salary </strong></td>
    <td>&nbsp;</td>
    <td valign="middle" bgcolor="#e1e1e1"><input name="jobsal" type="text" size="80" value="<?php echo (isset($job['jobsal']) ? $job['jobsal'] : ''); ?>" /></td>
  </tr>
  <tr>
    <td align="right" valign="top" >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td height="29" align="right" valign="top" bgcolor="#e1e1e1" ><strong>Location</strong></td>
    <td width="4">&nbsp;</td>
    <td width="480" valign="middle" bgcolor="#e1e1e1">
		<?php $jobloc = (isset($job['jobloc']) ? $job['jobloc'] : 'Singapore'); ?>
		<select name="jobloc" id="jobloc" type="text" size="5">
			<option value="Singapore" <?php echo ($jobloc=='Singapore' ? ' selected ' : '');?> >Singapore</option>
			<option value="Others"    <?php echo ($jobloc=='Others' ? ' selected ' : ''); ?> >Others</option>
	     </select></td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td height="30" align="right" valign="top" bgcolor="#e1e1e1"><strong>Job Type </strong></td>
    <td>&nbsp;</td>
    <td valign="middle" bgcolor="#e1e1e1">
		<select name="jobtype" id="jobtype" type="text" size="5">
			<?php $jobtype = (isset($_POST['jobtype']) ? $_POST['jobtype'] : 'Full-Time/Permanent'); ?>
			<option value="FullTime/Permanent" <?php echo ($jobtype=='Full-Time/Permanent' ? ' selected ' : '')?>>FullTime/Permanent</option>
			<option value="Part-Time" <?php echo ($jobtype=='Part-Time' ? ' selected ' : '')?>> Part-Time</option>
			<option value="Contract" <?php echo ($jobtype=='Contract' ? ' selected ' : '')?>> Contract</option>
			<option value="Temp" <?php echo ($jobtype=='Temp' ? ' selected ' : '')?>> Temp</option>
		</select></td>
  </tr>
  <tr>
    <td colspan='3' align="right">&nbsp;</td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td height="32" align="right" valign="middle" bgcolor="#e1e1e1"><strong> Send Resumes to<font color=red>*</font></strong></td>
    <td>&nbsp;</td>
    <td valign="middle" bgcolor="#e1e1e1"> 
      <input name="jobsendto" type="text" size="60" maxlength="255" value="<?php echo (isset($job["jobsendto"]) ? $job["jobsendto"] : ''); ?>" />
      <strong>(Email address)</strong>    </td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td align="right" valign="top"><strong>Job Category</strong><font color=red> * *</font></td>
    <td valign="top">&nbsp;</td>
    <td>	
	<?php if ($_GET['act']=='add') { ?>
	<table width="474" cellpadding="0" cellspacing="0">
      <tr>
        <td width="52%">
            <input type="radio" name="cat" value="Accounting">
        Accounting </td>
        <td width="48%"><input type="radio" name="cat" value="Human_Resource">
Human Resource </td>
        </tr>
      <tr>
        <td width="52%">
            <input type="radio" name="cat" value="Administration">
        Administration </td>
        <td width="48%"><input type="radio" name="cat" value="IT">
  IT </td>
      </tr>
      <tr>
        <td width="52%">
            <input type="radio" name="cat" value="Advertising_and_Communications">
        Advertising and Communications </td>
        <td width="48%"><input type="radio" name="cat" value="Junior_Executives">
  Junior Executives </td>
      </tr>
      <tr>
        <td width="52%">
            <input type="radio" name="cat" value="Banking_and_Finance">
        Banking and Finance </td>
        <td width="48%"><input type="radio" name="cat" value="Life_Services">
  Life Services </td>
      </tr>
      <tr>
        <td width="52%">
            <input type="radio" name="cat" value="Building-Construction">
        Building/Construction </td>
        <td width="48%"><input type="radio" name="cat" value="Management">
  Management </td>
      </tr>
      <tr>
        <td width="52%"><input type="radio" name="cat" value="Customer_Service">
        Customer Service </td>
        <td width="48%"><input type="radio" name="cat" value="Manufacturing">
  Manufacturing </td>
      </tr>
      <tr>
        <td width="52%">
            <input type="radio" name="cat" value="Design">
        Design </td>
        <td width="48%"><input type="radio" name="cat" value="Marketing-Public_Relations">
  Marketing/Public Relations </td>
      </tr>
      <tr>
        <td><input type="radio" name="cat" value="Education-Training">
Education/Training </td>
        <td width="48%"><input type="radio" name="cat" value="Others">
  Others </td>
      </tr>
      <tr>
        <td><input type="radio" name="cat" value="Engineering">
Engineering </td>
        <td width="48%"><input type="radio" name="cat" value="Research_and_Survey">
  Research and Survey </td>
      </tr>
      <tr>
        <td width="52%"><input type="radio" name="cat" value="Executives">
  Executives </td>
        <td width="48%"><input type="radio" name="cat" value="Sales">
  Sales </td>
      </tr>
      <tr>
        <td width="52%"><input type="radio" name="cat" value="Health_and_Medical_Services">
  Health and Medical Services </td>
        <td width="48%"><input type="radio" name="cat" value="Supply_Chain">
  Supply Chain</td>
      </tr>
      <tr>
        <td width="52%"><input type="radio" name="cat" value="Hospitality_Services">
Hospitality Services </td>
        <td width="48%"><input type="radio" name="cat" value="Telecommunications">
  Telecommunications </td>
      </tr>
    </table>
	<?php } else { echo $job["cat"]; echo "<input type=hidden id='cat' name ='cat' value='".$job["cat"]."'>"; } ?>
	</td>
  </tr>
  <tr>
    <td >&nbsp;</td><td></td></td><td width="3"></td>
  </tr>
  <tr bgcolor="#e1e1e1">
    <td height="51" align="right" valign="middle" bgcolor="#e1e1e1"><strong>Notes</strong></td>
    <td>&nbsp;</td>
    <td><font color=red>*</font> - Required<br>
      <font color=red>* *</font> - Please make sure you enter category. You will not be able to change job category later. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr><td>&nbsp;</td><td>&nbsp;</td>
    <td><input name="backButton" type="button" onClick="window.history.go(-1);" value="Back">
		<input type="button" name="Submit" value="Submit" onclick="return formValidate();">
<?php if ($_GET["act"] == "edit"): ?>
		<input type="button" value="Delete" onclick="javascript:window.location='jobadmin.php?act=del&cat=<?php echo $cat; ?>&id=<?php echo $id; ?>'" />
<?php endif; ?>
</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>