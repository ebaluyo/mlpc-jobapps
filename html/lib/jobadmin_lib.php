<?php
require "lib/xmllib2.php";
require "lib/xmlfuncs.php";
require "lib/html_lib.php";
switch(@$_GET["act"]):
	case "add":
		if (count($_POST) > 0) {
			if (
				!empty($_POST["jobtitle"])
				&& !empty($_POST["jobdesc"])
				&& !empty($_POST["jobreq"])
				)
			{
				$xmlfile = "xml/".$_POST["cat"].".xml";
				$jobs = array();
				if (file_exists($xmlfile)) {
					//backup old file
					backup_xmlfile($xmlfile);
					
					$contents = file_get_contents($xmlfile);
					if (trim($contents)=="") {
						$id = 2;
						#$jobsxml["jobs"]["job"][] = "DO NOT REMOVE"; 
                        $job = array('id'=>1, 'jobtitle'=>"DO NOT REMOVE");
						$jobs[] = $job;
					} else {
						$jobsxml = XML_unserialize($contents);
						$id = $jobsxml["jobs attr"]["last_id"] + 1;
						$jobs = $jobsxml["jobs"]["job"];
						//echo 'HERE<br/>';
					}
				} else {
					//echo 'file does not exist.';
					$id = 2;
					//$jobsxml["jobs"]["job"][] = "DO NOT REMOVE"; 
                    
                    $job = array('id'=>1, 'jobtitle'=>"DO NOT REMOVE");
					$jobs[] = $job;
					
					//d($jobs, 0);
					
					// problem occurs if only 1 entry - eng
					// nagloloko kasi siya pag isa lang yung entry tapos nag-unserialize - tagalog
				}
				
				foreach ($_POST as $key =>$value) {
					$_POST[$key] = str_replace('�',"", $_POST[$key]);
					$_POST[$key] = str_replace('�',"", $_POST[$key]);
					$_POST[$key] = str_replace('�','"', $_POST[$key]);
					$_POST[$key] = str_replace('�','"', $_POST[$key]);
                    
                    // $_POST[$key] = htmlspecialchars($_POST[$key], ENT_QUOTES); //any other special chars?
					if ($key != 'cat') $_POST[$key] = str_replace('-','', $_POST[$key]);

					$_POST[$key] = addslashes($value);
					
					#$_POST[$key] = htmlspecialchars($value);
					$_POST[$key] = htmlentities($_POST[$key]) ; #, ENT_QUOTES);					
				}
				
				$job = $_POST;
				$job["id"] = $id;
				$job["last_updated"] = time();
				$jobsxml["jobs attr"]["last_id"] = $id;
				
				#d($jobsxml["jobs"]["job"], 1);	#echo 'BEFORE:';d($jobs, 0); d($job, 0);
				array_push ($jobs, $job);
				
				#echo 'AFTER:';d($jobs, 1);

				$jobsxml["jobs"]["job"] = $jobs;
				//echo 'AFTER:';d($jobsxml, 1);
				
				$fp = fopen($xmlfile, "w+");
				if (fwrite($fp, XML_serialize($jobsxml)))
					echo "<div style=\"text-align: center\"><a href=\"?act=edit&cat=".$job["cat"]."&id=".$job["id"]."\">Job added.</a></div><br /><br />";
				else
					die("Unexplainable error!");
				fclose($fp);
			} else echo "<div style=\"color: red; text-align: center\">Please fill up all required fields.</div><br /><br />";
		}
		include("lib/jobs_form.php");
	break;
	case "edit":
		#d($_GET,0);
		#d($_POST,0);
		if (count($_POST) > 0) {
			if (
				!empty($_POST["jobtitle"])
				&& !empty($_POST["jobdesc"])
				&& !empty($_POST["jobreq"])
				)
			{
				foreach ($_POST as $key =>$value) {
					$_POST[$key] = str_replace('�',"", $_POST[$key]);
					$_POST[$key] = str_replace('�',"", $_POST[$key]);
					$_POST[$key] = str_replace('�','"', $_POST[$key]);
					$_POST[$key] = str_replace('�','"', $_POST[$key]);

					if ($key != 'cat') $_POST[$key] = str_replace('-','', $_POST[$key]);
				
					$_POST[$key] = addslashes($value);
					#$_POST[$key] = htmlspecialchars($value);					
					$_POST[$key] = htmlentities($_POST[$key]) ; #, ENT_QUOTES);					
				}
				
				#d($_POST);
				// at this point, it is assumed that the file exist [because we can edit..]
				
				$xmlfile = "xml/".$_POST["cat"].".xml";
				$jobsxml = XML_unserialize(file_get_contents($xmlfile));
				$job = $_POST;
				$job["last_updated"] = time();
				
				$jobarray = $jobsxml["jobs"]["job"];
				
				foreach ($jobarray as $key => $_job) {
					if ($_job["id"] == $job["id"])
						$jobarray[$key] = $job;
				}
				//d($jobarray); 
				
				$jobsxml["jobs"]["job"] = $jobarray;
				
				//backup old file
				backup_xmlfile($xmlfile);
				
				$fp = fopen($xmlfile, "w+");
				if (fwrite($fp, XML_serialize($jobsxml))) {
					#header("Location: jobadmin.php?act=edit&cat=".$job["cat"]."&id=".$job["id"]."&edit=true");
					$link = "jobadmin.php?act=edit&cat=".$job["cat"]."&id=".$job["id"]."&edit=true";
					echo '<script type="text/javascript">window.onload = function () { alert(\'Edit Successfull.\'); window.location = "'.$link.'"; } </script>';
				} else
					die("File write error!");
				fclose($fp);
			} else echo "<div style=\"color: red; text-align: center\">Please fill up all required fields.</div><br /><br />";
		} else {
			$cat = @$_GET["cat"];
			$id = @$_GET["id"];
			if (@$_GET["edit"])
				echo "<div style=\"text-align: center\">Job edited.</div><br /><br />";
			include("lib/jobs_form.php");
		}
	break;
	case "del":
		$cat = @$_GET["cat"];
		$id = @$_GET["id"];
		if ($_GET["act"] == "del" && (empty($cat) || empty($id))) die("Incomplete query.");
		if (@$_POST["del"]) {
			$xmlfile = "xml/".$cat.".xml";
			$jobsxml = XML_unserialize(file_get_contents($xmlfile));
			$_jobs = array();
			foreach ($jobsxml["jobs"]["job"] as $key => $job) {
				if ($job["id"] != $id)
					$_jobs[$key] = $jobsxml["jobs"]["job"][$key];
			}
			
			//backup old file
			backup_xmlfile($xmlfile);
					
			if (count($_jobs) > 1) {
				$jobsxml["jobs"]["job"] = $_jobs;
				$fp = fopen($xmlfile, "w+");
				if (fwrite($fp, XML_serialize($jobsxml))) {
					#header("Location: jobadmin.php?cat=".$cat);
					echo '<script type="text/javascript">window.onload = function () { alert(\'Delete successfull.\'); window.location = "'."jobadmin.php?cat=".$cat.'"; } </script>';
				}
			} else unlink($xmlfile);
		} else {
?>
	<div style="text-align: center">
		<form action="jobadmin.php?act=del&amp;cat=<?php echo $cat; ?>&amp;id=<?php echo $id; ?>" method="post">
			Are you sure you want to delete this job?<br />
			<input type="hidden" name="del" value="1" />
			<input type="submit" value="Yes" />
			<input type="button" value="No" onclick="window.history.go(-1);" />
		</form>
	</div><br /><br />
<?php
		}
	break;
	default:
		$dir = "xml/";
		if ($dh = opendir($dir)) {
			$onchange = ' window.location = \'?cat=\' + this.value; ';
			echo "<form action=\"jobadmin.php\" method=\"get\"><select name=\"cat\" onchange=\"".$onchange."\">\n";
			$getCat = isset($_GET['cat']) ? $_GET['cat'] : '';
			while (($xmlfile = readdir($dh)) !== false) {
				if (is_cat_file($xmlfile)) {
					$cat = substr($xmlfile, 0, -4);
					$catf = format_cat($cat);
					echo "\t<option value=\"$cat\" ".($cat == $getCat ? ' selected="selected" ' : '').">$catf</option>\n";
				}
			}
			echo "</select>\n<input type=\"submit\" value=\"Browse\" /></form>\n";
			closedir($dh);
		} else die("xml dir is missing!");
		
		if (!empty($_GET["cat"])):
			$jobs = get_jobs($_GET["cat"]);
			if (!$jobs) echo "<div style=\"color: red; text-align: center\">Category does not exist or has no entries</div><br /><br />";
			else {
?>
<br />
<table width="100%" cellspacing="0" cellpadding="0" class="font4" border="1">
	<tr align="left" bgcolor="#e1e1e1">
		<th width="33%">Job Title</th>
		<th>Job Description</th>
	</tr>
<?php		$bgColor = '#e1e1e1';
			foreach ($jobs as $job):
				$id = $job["id"];
				$bgColor = ($bgColor == '#e1e1e1' ? '' : '#e1e1e1');
?>
	<tr bgcolor='<?php echo $bgColor?>'>
		<td valign="top"><a href="?act=edit&amp;cat=<?php echo $job["cat"]; ?>&amp;id=<?php echo $job["id"]; ?>"><?php echo $job["jobtitle"]; ?></a></td>
		<td valign="top"><?php echo newline_convert(html_entity_decode(stripslashes($job["jobdesc"]))); ?></td>
	</tr>
<?php
			endforeach;
?>
</table>
<?php
			}
		else:
			// no cat? do nothing
		endif;
	break;
endswitch;
?>
