<?php
/* WorldRemit - GlobalSpan Project Management System
 * html_lib.php
 * Version: 1.1
 * Last Edit: April 4, 2001
 * By: Floyd Piedad
 * This is a library of functions applicable to HTML processing
 *
 */


/******************************************************************************
NEWLINE_CONVERT: changes newline characters to HTML <BR> tags.  This code is OS 
   compatible with MAC, Windows, or Unix.
 *****************************************************************************/
function newline_convert($text)
	{
	$new_text = preg_replace("/(\015\012)|(\015)|(\012)/","<br />",$text); 
	return $new_text;
	}

/******************************************************************************
RISKYHTML: allows certain html tags and prevents others from being stored.  Returns the clean version of the string or text.
 *****************************************************************************/
function riskyhtml($str) 
{ 
  $approvedtags = array( 
    "p"=>2,   // 2 means accept all qualifiers: <foo bar> 
    "b"=>1,   // 1 means accept the tag only: <foo> 
    "i"=>1, 
    "a"=>2,
    "div"=>2,
    "pre"=>1,
    "table"=>2,
    "td"=>2,
    "tr"=>2,
    "font"=>2,
    "em"=>1, 
    "br"=>1, 
    "strong"=>1, 
    "blockquote"=>1, 
    "tt"=>1, 
    "hr"=>1, 
    "li"=>1,
    "dir"=>1,
    "ol"=>1, 
    "ul"=>1 
    ); 

  $str = stripslashes($str); 
  $str = eregi_replace("<[[:space:]]*([^>]*)[[:space:]]*>","<\\1>",$str); 
  $str = eregi_replace("<a([^>]*)href=\"?([^\"]*)\"?([^>]*)>", 
    "<a href=\"\\2\">", $str); 
  $tmp = ""; 
  while (eregi("<([^> ]*)([^>]*)>",$str,$reg)) 
  { 
    $i = strpos($str,$reg[0]); 
    $l = strlen($reg[0]); 
    if ($reg[1][0] == "/") 
      $tag = strtolower(substr($reg[1],1)); 
    else 
      $tag = strtolower($reg[1]); 
    if ($a = $approvedtags[$tag]) 
      if ($reg[1][0] == "/") 
        $tag = "</$tag>"; 
      elseif ($a == 1) 
        $tag = "<$tag>"; 
      else 
        $tag = "<$tag " . $reg[2] . ">"; 
    else 
      $tag = ""; 
    $tmp .= substr($str,0,$i) . $tag; 
    $str = substr($str,$i+$l); 
  } 
  $str = $tmp . $str; 

  // Squash PHP tags unconditionally 
  $str = ereg_replace("<\?","",$str); 
  $str = addslashes($str);
  return $str; 
} 


/******************************************************************************
SAFEHTML: allows certain html tags and prevents others from being stored.  Returns the clean version of the string or text.
 *****************************************************************************/
function safehtml($str) 
{ 
  $approvedtags = array( 
    "p"=>2,   // 2 means accept all qualifiers: <foo bar> 
    "b"=>1,   // 1 means accept the tag only: <foo> 
    "i"=>1, 
    "a"=>2, 
    "em"=>1, 
    "br"=>1, 
    "strong"=>1, 
    "blockquote"=>1, 
    "tt"=>1, 
    "hr"=>1, 
    "li"=>1, 
    "ol"=>1, 
    "ul"=>1 
    ); 

  $str = stripslashes($str); 
  
  //$str = eregi_replace("<[[:space:]]*([^>]*)[[:space:]]*>","<\\1>",$str); 
  //$str = eregi_replace("<a([^>]*)href=\"?([^\"]*)\"?([^>]*)>", "<a href=\"\\2\">", $str); 

  $str = preg_replace("/<[[:space:]]*([^>]*)[[:space:]]*>/","<\\1>",$str); 
  $str = preg_replace("/<a([^>]*)href=\"?([^\"]*)\"?([^>]*)>/", "<a href=\"\\2\">", $str); 

  $tmp = ""; 
  //while (eregi("<([^> ]*)([^>]*)>",$str,$reg)) 
  while (preg_match("/<([^> ]*)([^>]*)>/i",$str,$reg)) 
  { 
    $i = strpos($str,$reg[0]); 
    $l = strlen($reg[0]); 
    if ($reg[1][0] == "/") 
      $tag = strtolower(substr($reg[1],1)); 
    else 
      $tag = strtolower($reg[1]); 
    if ($a = $approvedtags[$tag]) 
      if ($reg[1][0] == "/") 
        $tag = "</$tag>"; 
      elseif ($a == 1) 
        $tag = "<$tag>"; 
      else 
        $tag = "<$tag " . $reg[2] . ">"; 
    else 
      $tag = ""; 
    $tmp .= substr($str,0,$i) . $tag; 
    $str = substr($str,$i+$l); 
  } 
  $str = $tmp . $str; 

  // Squash PHP tags unconditionally 
  //$str = ereg_replace("<\?","",$str); 
  $str = preg_replace("/<\?/","",$str); 

  $str = addslashes($str);
  return $str; 
} 

/******************************************************************************
MINHTML: allows certain html tags and prevents others from being stored.  Returns the clean version of the string or text.
 *****************************************************************************/
function minhtml($str) 
{ 
  $approvedtags = array( 
    "b"=>1,   // 1 means accept the tag only: <foo> 
    "i"=>1, 
    "br"=>1, 
    "li"=>1, 
    "ol"=>1, 
    "ul"=>1 
    ); 

  $str = stripslashes($str); 
  $str = eregi_replace("<[[:space:]]*([^>]*)[[:space:]]*>","<\\1>",$str); 
  $str = eregi_replace("<a([^>]*)href=\"?([^\"]*)\"?([^>]*)>", 
    "<a href=\"\\2\">", $str); 
  $tmp = ""; 
  while (eregi("<([^> ]*)([^>]*)>",$str,$reg)) 
  { 
    $i = strpos($str,$reg[0]); 
    $l = strlen($reg[0]); 
    if ($reg[1][0] == "/") 
      $tag = strtolower(substr($reg[1],1)); 
    else 
      $tag = strtolower($reg[1]); 
    if ($a = $approvedtags[$tag]) 
      if ($reg[1][0] == "/") 
        $tag = "</$tag>"; 
      elseif ($a == 1) 
        $tag = "<$tag>"; 
      else 
        $tag = "<$tag " . $reg[2] . ">"; 
    else 
      $tag = ""; 
    $tmp .= substr($str,0,$i) . $tag; 
    $str = substr($str,$i+$l); 
  } 
  $str = $tmp . $str; 

  // Squash PHP tags unconditionally 
  $str = ereg_replace("<\?","",$str); 
  $str = addslashes($str);
  return $str; 
} 

/******************************************************************************
LEASTHTML: removes all html tags except bold and italics.  Returns the clean version of the string or text.
 *****************************************************************************/
function leasthtml($str) 
{ 
  $approvedtags = array( 
    "b"=>1,   // 1 means accept the tag only: <foo> 
    "i"=>1
    ); 

  $str = stripslashes($str); 
  $str = eregi_replace("<[[:space:]]*([^>]*)[[:space:]]*>","<\\1>",$str); 
  $str = eregi_replace("<a([^>]*)href=\"?([^\"]*)\"?([^>]*)>", 
    "<a href=\"\\2\">", $str); 
  $tmp = ""; 
  while (eregi("<([^> ]*)([^>]*)>",$str,$reg)) 
  { 
    $i = strpos($str,$reg[0]); 
    $l = strlen($reg[0]); 
    if ($reg[1][0] == "/") 
      $tag = strtolower(substr($reg[1],1)); 
    else 
      $tag = strtolower($reg[1]); 
    if ($a = $approvedtags[$tag]) 
      if ($reg[1][0] == "/") 
        $tag = "</$tag>"; 
      elseif ($a == 1) 
        $tag = "<$tag>"; 
      else 
        $tag = "<$tag " . $reg[2] . ">"; 
    else 
      $tag = ""; 
    $tmp .= substr($str,0,$i) . $tag; 
    $str = substr($str,$i+$l); 
  } 
  $str = $tmp . $str; 

  // Squash PHP tags unconditionally 
  $str = ereg_replace("<\?","",$str); 
  $str = addslashes($str);
  return $str; 
} 

?>