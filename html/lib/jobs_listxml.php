<?php
	$cat = $_GET["jobcat"];
	if ($cat != "all") echo "Category :: ".format_cat($cat)." <br><hr/><br>";
?>
<table width="100%" cellpadding="0" cellspacing="0" align='center'>
<tr>
	<th width="20%">Title</th>
	<th width="20%">Location</th>
	<th width="20%">Job Function</th>
	<th width="20%">Employment</th>
	<th width="20%">Last Updated</th>
</tr>
<?php
    $jobs = get_jobs($cat); 
    //d($jobs);
	$c = count($jobs); //echo $c;//die;
	$found = array();
	if ($c > 0 ) {
        //d($jobs, 0);

		foreach ($jobs as $key => $job) {
			$locfound = 0; 
            
            if (!empty($jobkw))
				$kwfound = strpos($job['jobdesc'], $jobkw);
			else $kwfound = 1;
			
			if ($jobloc != "any") {
				if (!isset($job['jobloc'])) 
					$locfound = 1;
				else
					$locfound = ($job['jobloc'] == $jobloc);
				
			} else $locfound = 1;
			
			//var_dump($job);
			if (empty($job['jobtitle']) || is_null($job['jobtitle']) || is_null($job['id']))
				$locfound = 0;

			if ($kwfound && $locfound) {
                //$job['locfound'] = $locfound;
                //$job['found'] = $kwfound;
				$found[] = $job;
                
                //echo 'Key' . $key . ' , Count:' . count($job) . '--------------------------' . "\n"; 
                //if (count ($job) == 1 ) echo print_r($job, true) . '--------------------------' . "\n"; 
                // die;
			}		
		}
        
        // print_r($found); 
        //die;
        if (count($found) > 0) {
            
			$odd = true;
			foreach ($found as $job) {
                $last_updated = isset($job["last_updated"]) ? $job["last_updated"] : 0;
                //if ($last_updated == 0) continue;
				echo "<tr" . ($odd ? " bgcolor=\"#e1e1e1\"" : "") .">" . 
					"<td><a href=\"jobview.php?cat=" . $job["cat"] . "&id=" . $job["id"] ."\">". $job["jobtitle"] . "</a></td>" . 
					"<td>".(isset($job["jobloc"]) ? $job["jobloc"] : ''). "</td>". 
					"<td>".format_cat($job["cat"])."</td>". 
					"<td>".(isset($job["jobtype"]) ? $job["jobtype"] : '')."</td>". 
					"<td>".date("m/j/Y g:i A", intval($job["last_updated"]))."</td>".
					// ( $last_updated == 0 ? "<td>".print_r($job, true)."</td>" : '').
                    "</tr>";
				$odd = !$odd;
			}
		} else echo "<tr><td colspan=\"3\">No entries found.</td></tr>";
	} else echo "<tr><td colspan=\"3\">Category does not exist or has no entries.</td></tr>";
?>
</table>
